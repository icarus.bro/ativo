## Inicializando o projeto
### Caso tenha algo a adcionar a este README não exite em colaborar.

```bash
$ git clone https://<you>@bitbucket.org/dev-esferabr/bancodobrasil-hibrido.git
```

### Clone realizado:

Tenha certeza de que o Node instalado na sua máquina seja Node 6 LTS e NPM 3+, como mencionado na documentação [doc CLI] (https://ionicframework.com/docs/cli/).

*Caso não tenha ionic CLI instalado 

```bash
$ npm install -g ionic@latest 
```

*Clone realizado, CLI atualizado. Execute o seguinte comando:
*Também irá instalar as dependêcias do projeto 

```bash
$ ionic serve
```
## Ferramentas

Android Studio: a sdk necessária para o desenvolvimento acompanha a instalção da IDE.

### Criação de uma AVD via terminal, para o emulador

*Acesse o diretório. 
*Para que ocorra como o esperado a stack de estar instalada: `java jdk` `android sdk`. Com as variáveis devidamente setadas

`C:\Users\icarus\AppData\Local\Android\sdk\tools>`

*Execute

```bash
$ android create avd -n <name-avd> -k "system-images;android-25;google_apis;x86"
```

##Comandos úteis para gerenciamento de AVDS
[Referencia] (https://developer.android.com/studio/command-line/avdmanager.html)

* lista AVDs

```bash
$ android list avd
```

* deleta AVD

```bash
$ android delete avd -n <name-avd>
```

Todos os comandos estão na refêrencia. [Commands List](https://ionicframework.com/docs/cli/commands.html).

### Hakcs
DEBUG
* Problema
Duranto o desenvolvimento o browser não consegiu acessar o cordova.js. 

* Solução
O google chrome é capaz de mapear os dispositivos em execução na máquina, e isso inclui AVDs, através das ferramentas de desenvolvedor. 

Customize and control DevTools > More tools > Remote Devices 

Pronto! Escolha o device criado e debug e click no botão `inpect`. 

### Chave de upload para assinatura
*bancodobrasil-hibrido\release\corrida-bb-key.jks



