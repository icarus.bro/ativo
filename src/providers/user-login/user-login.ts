import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import {Md5} from 'ts-md5/dist/md5';
import { User } from '../../models/user-model';
import { Events } from 'ionic-angular';
import { MenuProvider } from '../menu/menu';

import { Storage } from '@ionic/storage';

/*
  Generated class for the UserLoginProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserLoginProvider {

  prefix: string = 'http://webservices.esferabr.com.br/Ativo/';

  user = new User();

  constructor(public http: Http,
    private storage: Storage,
    private menuProvider: MenuProvider,
    private event: Events,
    // private util: Util
  ) {
    console.log('Hello UserLoginProvider Provider');

    this.storage.get('user').then((val) => {
      if(val){
        this.user = val;
        console.log("Usuário logado")


        this.menuProvider.check(this.user.name);
        this.event.publish('changeLoginButton',this.user.name);
      }
    });
  }


  isLogin(): boolean {
    return this.user.name != ''
  }


  login(email: string, senha: string){
    let pswCript = Md5.hashStr(senha);
		this.user.password = pswCript;
		console.log( this.user.password );


    return this.http.get(this.prefix + 'AutenticaUsuario?user=' + email + '&pass=' + pswCript);
  }

  getToken(email, senha){
      let pswCript = Md5.hashStr(senha);
       return this.http.get(this.prefix + 'getToken?usuario=' + email + '&senha=' + pswCript);
  }

  cleanUser(){
    this.user = new User();
    this.storage.clear();
    this.event.publish('changeLoginButton',this.user.name);
  }

  populateUser(user, email){
    this.user.idUser = user.id_usuario;
    this.user.name = user.ds_nomecompleto;
    this.user.celular = user.nr_celular;
    this.user.dataCadastro = user.dt_cadastro;
    this.user.dataNascimento = new Date(user.dt_nascimento);
    // console.log(user.dt_nascimento)
    // console.log(this.user.dataNascimento)
    this.user.residencial = user.nr_telefone;
    this.user.sexo = user.fl_sexo;
    this.user.email = email;
    this.user.docNumber = user.nr_documento;
    this.user.docType = user.id_tipo_documento;


    if(user.ds_info_facebook == "1"){
      this.user.facebook = true
    }else{
      this.user.facebook = false
    }

    if(user.fl_newsletter == "1"){
      this.user.ReceiveEmail = true
    }else{
      this.user.ReceiveEmail = false
    }

    if(user.ds_info_sms == "1"){
      this.user.sms = true
    }else{
      this.user.sms = false
    }

    if(user.nm_necessidades_especiais == "N"){
      this.user.specialNeeds = false;
    }else{
      this.user.specialNeeds = true;
    }

    this.user.parentName = user.ds_nome_contato_e;
    this.user.parentContact = user.nr_telefone_contato_e;

    // this.user.endereco.cep = user.ds_cep;
    // this.user.endereco.cidade = user.ds_cidade;
    // this.user.endereco.cidadeId = user.id_cidade;
    // this.user.endereco.pais = user.ds_pais;
    // this.user.endereco.paisId = user.id_pais;
    // this.user.endereco.estado = user.ds_estado;
    // this.user.endereco.estadoId = user.id_estado;
    // this.user.endereco.bairro = user.ds_bairro;
    // this.user.endereco.endereco = user.ds_endereco;
    // this.user.endereco.numero = user.nr_numero;
    // this.user.endereco.complemento = user.ds_complemento;
    //console.log(this.user)
  }

  // renewUser(user, idUser?){

  //   //console.log(user)

  //   //person
  //   let person = user.person;
  //   this.user.idUser = idUser ? idUser : this.user.idUser;
  //   this.user.name = person.name;
  //   this.user.docType = person.typeDoc;
  //   if(this.user.docType == "1" || this.user.docType == "0"){
  //     this.user.docNumber = this.util.cleanCPF(person.numberDoc);
  //   }else{
  //     this.user.docNumber = person.numberDoc;
  //   }

  //   this.user.sexo = person.genre;
  //   this.user.dataNascimento = new Date(person.birthday);
  //   this.user.specialNeeds = person.specialNeeds;

  //   //adress
  //   let adress = user.adress ? user.adress : "";

  //   this.user.endereco.cep          = adress.postalCode        ? adress.postalCode        : ""
  //   this.user.endereco.paisId       = adress.country.id        ? adress.country.id        : ""
  //   this.user.endereco.cidadeId     = adress.city.id           ? adress.city.id           : ""
  //   this.user.endereco.estadoId     = adress.state.id          ? adress.state.id          : ""
  //   this.user.endereco.numero       = adress.number            ? adress.number            : ""
  //   this.user.endereco.bairro       = adress.neighborhood      ? adress.neighborhood      : ""
  //   this.user.endereco.complemento  = adress.complement        ? adress.complement        : ""
  //   this.user.endereco.endereco     = adress.adress            ? adress.adress            : ""

  //   //contact
  //   let contact = user.contact;

  //   this.user.celular = contact.celphone
  //   this.user.email = contact.email
  //   this.user.parentContact = contact.parentPhone
  //   this.user.parentName = contact.parentName
  //   this.user.residencial = contact.telephone

  //   //news
  //   let news = user.news;

  //   this.user.sms = news.sms;
  //   this.user.facebook = news.facebook;
  //   this.user.ReceiveEmail = news.email;

  //   // this.menuProvider.check(this.user.name);
  //   // this.event.publish('changeLoginButton',this.user.name);

  //   console.log(this.user)

  //   this.storage.set('user', this.user);

  // }

  // createUserToFriend(user, adress){
  //   let url = "http://webservices.esferabr.com.br/AppAtivo/User";

  //   let body = {};

  //   body["ds_nome"] = user.name       ?  user.name.substr(0,user.name.indexOf(' '))   : null;
  //   body["ds_sobrenome"] = user.name  ?  user.name.substr(user.name.indexOf(' ')+1)   : null;

  //   body["id_tipo_documento"] = user.typeDoc    ? this.toDocId(user.typeDoc)          : null;
  //   if(this.user.docType == "1" || this.user.docType == "0"){
  //     body["nr_documento"]      = user.numberDoc  ? this.util.cleanCPF(user.numberDoc)  : null;
  //   }else{
  //     body["nr_documento"]      = user.numberDoc  ? user.numberDoc  : null;
  //   }
  //   body["fl_sexo"]           = user.genre      ? user.genre                          : null;
  //   body["dt_nascimento"]     = user.birthday   ? user.birthday                       : null;
  //   body["ds_email"]          = user.email      ? user.email                          : null;

  //   body["ds_cep"]          = adress.postalCode        ? adress.postalCode        : null;
  //   body["id_pais"]         = adress.country.id        ? adress.country.id        : null;
  //   body["nr_numero"]       = adress.number            ? adress.number            : null;
  //   body["ds_bairro"]       = adress.neighborhood      ? adress.neighborhood      : null;
  //   body["id_estado"]       = adress.state.id          ? adress.state.id          : null;
  //   body["id_cidade"]       = adress.city.id           ? adress.city.id           : null;
  //   body["ds_complemento"]  = adress.adress.complement ? adress.adress.complement : null;
  //   body["ds_endereco"]     = adress.adress.adress     ? adress.adress.adress     : null;

  //   body["type"] = user.type;
  //   body["fl_tipo_pessoa"] = user.personType;
  //   body["id_tipo_usuario"] = user.userType;

  //   console.log(url);
  //   console.log(JSON.stringify(body));

  //   return this.http.post(url, JSON.stringify(body));
  // }

  // createUser(user, isNew){

  //   // console.log(user)
  //   // console.log(isNew)

  //   let url = "http://webservices.esferabr.com.br/AppAtivo/User";

  //   let body = this.populateJSON(user, isNew)

  //   //console.log(url);
  //   //console.log(body);
  //   return this.http.post(url, JSON.stringify(body));

  // }

  // populateJSON(user, isNew){

  //   let person = user.person;
  //   let adress = user.adress;
  //   let contact = user.contact;
  //   let news = user.news;

  //   let body = {};

  //   let data = new Date(person.birthday)
  //   let dataAdjusted = data.getFullYear() +"-"+ (data.getMonth()+1) +"-"+data.getDate()

  //   if(isNew == "update"){
  //     body["user_id"] = this.user.idUser;
  //     body["token"] = this.user.token;
  //     //console.log(this.user.token)
  //   }

  //   //person
  //   body["ds_nome"] = person.name       ?  person.name.substr(0,person.name.indexOf(' '))   : "";
  //   body["ds_sobrenome"] = person.name  ?  person.name.substr(person.name.indexOf(' ')+1)   : "";

  //   body["id_tipo_documento"] = person.typeDoc    ? person.typeDoc                        : "";
  //   body["nr_documento"]      = person.numberDoc  ? this.util.cleanCPF(person.numberDoc)  : "";
  //   body["fl_sexo"]           = person.genre      ? person.genre                          : "";
  //   body["dt_nascimento"]     = person.birthday   ? dataAdjusted                          : "";

  //   body["nm_necessidades_especiais"] = person.specialNeeds   ? person.specialNeeds       : false;

  //   //adress
  //   body["ds_cep"]          = adress.postalCode        ? adress.postalCode        : "";
  //   body["id_pais"]         = adress.country.id        ? adress.country.id        : "";
  //   body["nr_numero"]       = adress.number            ? adress.number            : "";
  //   body["ds_bairro"]       = adress.neighborhood      ? adress.neighborhood      : "";
  //   body["id_estado"]       = adress.state.id          ? adress.state.id          : "";
  //   body["id_cidade"]       = adress.city.id           ? adress.city.id           : "";
  //   body["ds_complemento"]  = adress.complement        ? adress.complement        : "";
  //   body["ds_endereco"]     = adress.adress            ? adress.adress            : "";

  //   //contact
  //   body["nr_celular"]            = contact.celphone       ? contact.celphone       : "";
  //   body["ds_email"]              = contact.email          ? contact.email         : "";

  //   if(contact.password =! "*@Ativo@*"){
  //     body["ds_senha"]              = contact.password       ? Md5.hashStr(contact.password)      : "";
  //   }
  //   body["nr_telefone"]           = contact.telephone      ? contact.telephone     : "";
  //   body["nr_telefone_contato_e"] = contact.parentPhone    ? contact.parentPhone   : "";
  //   body["ds_nome_contato_e"]     = contact.parentName     ? contact.parentName    : "";

  //   //news
  //   body["ds_info_sms"]       = news.sms       ? news.sms      : false;
  //   body["ds_info_facebook"]  = news.facebook  ? news.facebook : false;
  //   body["fl_newsletter"]     = news.email     ? news.email    : false;


  //   body["type"] = isNew;
  //   body["fl_tipo_pessoa"] = "F";
  //   body["id_tipo_usuario"] = "5";

  //   return body;
  // }

  resetPassword(email){
    let body = {};
    let url ="http://checkout.ativo.com/usuario/enviarsenha"

    body["tipo"]="email";
    body["ds_email"]= email;
    return this.http.post(url, JSON.stringify(body));
  }


  // getCountry(){
  //   return this.http.get("http://webservices.esferabr.com.br/AppAtivo/paisOrigem");
  // }

  // getState(countryId){
	// 	return this.http.get("http://webservices.esferabr.com.br/AppAtivo/getEstado?pais="+countryId);
  // }

  // getCity(StateId){
  //   return this.http.get("http://webservices.esferabr.com.br/AppAtivo/getCidades?estado="+StateId);
  // }

  // checkEmail(email){
  //   return this.http.get("http://webservices.esferabr.com.br/AppAtivo/checkUser?ds_email="+email)
  // }

  toDocId(docId){
    let id = 0
    switch(docId){
      case "cpf":
        id = 1;
        break;

      case "cpfResponsavel":
        id = 2;
        break;

      case "rne":
        id = 3;
        break;

      case "passaporte":
        id = 4;
        break;
    }
    return id;
  }


}
