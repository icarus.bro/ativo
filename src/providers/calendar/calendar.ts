import { Injectable } from '@angular/core';
import { Response, Http } from '@angular/http';
import { HttpService } from '../../services/http-service';

import { Util } from '../../util/util'
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

@Injectable()
export class CalendarProvider {

  constructor(private http: Http, private util: Util) {  }

  // public getModality(): Observable<Response> {

  //   let url = 'http://webservices.esferabr.com.br/Ativo/Modalidades';

  //   return this.http.get(url);
  // }

  public getEvents(specificSearch: any, categories?: Array<any>, mode?: string): Observable<Response> {
    // let url = "http://webservices.esferabr.com.br/Ativo/Evento?modo=bb";
    let url = "http://webservices.esferabr.com.br/Ativo/Evento?count=20&tipo=14"
    let parans = [];

    if(specificSearch.category == "14")
      categories = []

    let today = new Date();
    specificSearch.dateInitial = today.getFullYear()+"/"+(today.getMonth()+1)+"/"+today.getDate()

    mode                         ? parans.push('mode='   + mode)                                              : null;
    specificSearch.offset        ? parans.push('offset=' + specificSearch.offset)                             : null;
    specificSearch.local         ? parans.push('local='  + this.util.getClearCityName(specificSearch.local))  : null;
    specificSearch.category      ? parans.push('tipo='   + specificSearch.category)                           : null;
    categories.length            ? parans.push('modalidade=' + categories.join(','))                          : null;
    specificSearch.dateInitial   ? parans.push('dt_ini='   + specificSearch.dateInitial)                      : null;
    specificSearch.dateFinal     ? parans.push('dt_fim='   + specificSearch.dateFinal)                        : null;
    specificSearch.international ? parans.push('internacional='   + specificSearch.international)             : null;
    specificSearch.name          ? parans.push('pc=' + specificSearch.name)                                   : null;
    specificSearch.count         ? parans.push('count=' + specificSearch.count)                               : null;

    if(parans.length)
      url += '?';

    url += parans.join('&');

    console.log( "URL EVENTS" )
    console.log(url);

    return this.http.get(url);
  }


  public getEventsByDateRange(count : any, tipo :  any, city : any): Promise<Response> {
    let url = "http://webservices.esferabr.com.br/Mobile/Evento?count="+count+"&tipo="+tipo+"&pais=33&local="+city
    // let url = "http://webservices.esferabr.com.br/Ativo/Evento?";

    console.log("CIDADE---")
    console.log(city)

    return this.http.get(url).toPromise()
  }

  public getEventsByName(eventName : any): Promise<Response> {
    let url = "http://webservices.esferabr.com.br/Ativo/Evento?pc="+eventName;
    // let url = "http://webservices.esferabr.com.br/Ativo/Evento?";

    console.log("Nome do Evento - BUSCA---")
    console.log(eventName)

    return this.http.get(url).toPromise()
  }

  public getEventsByGeolocation(currentGeolocation): Promise<Response> {
    let url = "http://webservices.esferabr.com.br/Mobile/Evento?local="+currentGeolocation
    console.log(url)
    return this.http.get(url).toPromise()
  }



}
