import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { HttpService } from '../../services/http-service';

/*
  Generated class for the KitWithdrawProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class KitWithdrawProvider {

  constructor(public http: HttpService) {
  }

  public getKitsToWithdraw(userDates: any){
    let url = "http://webservices.esferabr.com.br/Ativo/meusKits?"
    let parans = [];
    
    userDates.userId    ?   parans.push("id_usuario="+userDates.userId) : null;
    userDates.offset    ?   parans.push("offset="+userDates.offset)     : null;
    parans.push("");
    // parans.push("modo=bb");

    url += parans.join('&');

    return this.http.get(url)
  }

  public getReceiptDetail(idPedido: any){
    return this.http.get("http://webservices.esferabr.com.br/Ativo/PedidoRecibo?id="+idPedido)
  }

  populateKitsToWithdraw(array: any){
    var toBack = [];
    for(let a of array){
      let kit = {
      idOrder: a.id_pedido,
      idCombo : a.id_combo,
      idEvent : a.id_evento,
      nameEvent : a.nome_evento,
      dateEvent: new Date(a.data_evento.split(" ")[0]+"T03:00:00"),
      adressEvent : a.end_evento,
      dsLocal : a.ds_local,
      stage : a.etapa,
      team : a.equipe,
      kitWithdraw : a.ds_retirada_kits,
      dsImageKit : a.ds_imagem_kit,
      idUser : a.id_usuario,
      receipt: a.recibo,
      }
      toBack.push(kit);
    }
    return toBack;
  }

}
