import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { UserLoginProvider } from '../user-login/user-login';
import { Pedido } from '../../models/pedido-model';
import { Pagamento } from '../../models/pagamento-model';
import { EventoAtleta } from '../../models/evento-atleta-model';

/*
  Generated class for the MeusPedidosProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MeusPedidosProvider {

  constructor(public http: Http,
              private user: UserLoginProvider) {
    console.log('Hello MeusPedidosProvider Provider');
  }

  //CODE - Banco do Brasil
  prefix: string = 'http://webservices.esferabr.com.br/Ativo/';

  getMeusPedidos(){
    // return this.http.get(this.prefix + 'meuspedidos?id_usuario='+this.user.user.idUser);
    return this.http.get(`${this.prefix}meuspedidos?id_usuario=${this.user.user.idUser}`);
    // &modo=bb`);
  }

  setMeusPedidos(pedidos: any){
    let populados = new Array<Pedido>()

    for(let pedido of pedidos){
      let p = new Pedido();

      p.id = pedido.id_pedido;
      p.data = pedido.data_pedido;
      p.idPedidoEvento = pedido.id_pedido__evento;
      p.taxaTotal = pedido.taxa_total;
      p.quantidadeEvento = pedido.qtd_evento;
      p.totalPedido = pedido.total_pedido;
      p.status = pedido.pedido_status;
      p.recibo = pedido.recibo;
      p.idUsuario = pedido.id_usuario;

      for (let inscrito of pedido.inscritos){
        p.eventoAtletas.push(this.setInscritos(inscrito))
      }

      for (let pag of pedido.pagamentos){
        p.pagamentos.push(this.setPagamentos(pag))
      }

      populados.push(p);
    }

    return populados;
  }

  setPagamentos(p){
    let pag = new Pagamento()
      pag.id = p.id_pedido;
      pag.valor = p.valor;
      pag.valorPago = p.valor_pago;
      pag.formaPagamento = p.forma_pagamento;
      pag.pagamentoStatus = p.pagamento_status;
    return pag;
  }

  setInscritos(inscrito){
    let insc = new EventoAtleta();

    insc.idPedido = inscrito.id_pedido;
    insc.dataPedido = inscrito.dt_pedido;
    insc.idUsuario = inscrito.id_usuario;
    insc.nome = inscrito.nome;
    insc.pelotao = inscrito.pelotao;
    insc.modalidade = inscrito.modalidade;
    insc.categoria = inscrito.categoria;
    insc.equipeRevezamento = inscrito.equipe_revezamento;
    insc.equipe = inscrito.equipe;
    insc.precoEvento = inscrito.preco_evento;
    insc.taxaEvento = inscrito.taxa_evento;
    insc.taxaDelivery = inscrito.taxa_delivery;
    insc.quantidadeEvento = inscrito.qtd_evento;
    insc.numeroQuantidade = inscrito.nm_qtd;
    insc.tamanhoCamiseta = inscrito.tamanho_camiseta;
    insc.numeroPeito = inscrito.nr_peito;
    insc.idEvento = inscrito.id_evento;
    insc.nomeEvento = inscrito.nome_evento;
    insc.dataEvento = inscrito.data_evento;
    insc.enderecoEvento = inscrito.end_evento;
    insc.etapa = inscrito.etapa;

    return insc;
  }


}
