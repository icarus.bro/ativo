
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Response } from '@angular/http';
import { HttpService } from '../../services/http-service';
import { Util } from '../../util/util';
import { SpecificAtlheteSeachModel } from '../../models/specific-athlete-search-model';
import { AtletaPosicaoModel } from '../../models/atleta-posicao-model';


/*
  Generated class for the ResultsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ResultsProvider {

  prefix = "http://webservices.esferabr.com.br/Ativo/"
  
    constructor(public http: HttpService, private util: Util) {
    }
  
    getAllPhotos(idEvento: number, offSet: number): Observable<Response>{
      var sufix = "EventosFotos?id_evento="+idEvento+"&offset="+offSet;
      return this.http.get(this.prefix+sufix);
    }
  
    // getAllPositions(idEvento: number, offSet: number): Observable<Response>{
    //   var sufix = "Resultado?id_evento="+idEvento+"&offset="+offSet;
    //   return this.http.get(this.prefix+sufix);
    // }
  
    getAllPositions(specificParams: SpecificAtlheteSeachModel): Observable<Response>{
      let url = "http://webservices.esferabr.com.br/Ativo/Resultado?"
      let parans = [];
  
      specificParams.idEvent    ? (parans.push("id_evento="+specificParams.idEvent))      :null;
      specificParams.numPeito   ? (parans.push("num_peito="+specificParams.numPeito))     :null;
      specificParams.name       ? (parans.push("nome="+specificParams.name))              :null;
      specificParams.categories ? (parans.push("categoria="+specificParams.categories))   :null;
      specificParams.team       ? (parans.push("equipe="+specificParams.team))            :null;
  
      if(specificParams.female && !specificParams.male){
        parans.push("sexo=f")
      }else if(!specificParams.female && specificParams.male){
        parans.push("sexo=m")
      }
      specificParams.offset     ? (parans.push("offset="+specificParams.offset))          :null;
  
      url += parans.join('&');
    
      return this.http.get(url)
    }
  
    getPositionsDetail(idEvento: number, numPeito: number): Observable<Response>{
      var sufix = "Resultado?id_evento="+idEvento+"&num_peito="+numPeito;
      return this.http.get(this.prefix+sufix);
    }
  
    getAthleteDetail(idEvento: number, idAthlete: number): Observable<Response>{
      var sufix = "ResultadoDetalhe?id="+idEvento+"&num_peito="+idAthlete
      return this.http.get(this.prefix+sufix);
    }
  
    // populatePhotoShowcase(toSerialize: Array<any>): Array<FotoResultadoModel>{
    //   var photoSerialized = new Array<FotoResultadoModel>();
  
    //   for(let item of toSerialize){
    //     var serialized = new FotoResultadoModel();
  
    //     serialized.idEventoCompreFotoItem = item.id_evento_compre_foto_item;
    //     serialized.idEventoCompreFoto = item.id_evento_compre_foto;
    //     serialized.nrPeito = item.nr_peito;
    //     serialized.dsAtleta = item.ds_atleta;
    //     serialized.dsEquipe = item.ds_equipe;
    //     serialized.flFotoNome = item.fl_foto_nome;
    //     serialized.idEvento = item.id_evento;
    //     serialized.qtd = item.qtd;
    //     serialized.dsFoto = item.ds_foto;
    //     serialized.etapa = item.etapa;
    //     serialized.etapa2 = item.etapa2;
    //     serialized.sexo = item.sexo;
    //     serialized.horario = item.horario;
    //     serialized.dtCadastro = item.dt_cadastro;
  
    //     photoSerialized.push(serialized)
    //   }
  
    //   return photoSerialized;
    // }
  
      populatePositionShowcase(toSerialize: Array<any>): Array<AtletaPosicaoModel>{
      var positionsSerialized = new Array<AtletaPosicaoModel>();
  
      for(let item of toSerialize){
        var serialized = new AtletaPosicaoModel();
  
        serialized.idUsuario = item.id_usuario;
        serialized.idEvento = item.id_evento;
        serialized.idResultado = item.id_resultado;
        serialized.nome = item.nome;
        serialized.sexo = item.sexo;
        serialized.numPeito = item.num_peito;
        serialized.modalidade = item.modalidade;
        serialized.percurso = item.percurso;
        serialized.categoria = item.categoria;
        serialized.equipe = item.equipe;
        serialized.tempoTotal = item.tempo_total;
        serialized.tempoBruto = item.tempo_bruto;
        serialized.pace = item.pace;
  
        serialized.itens.classificacaoCategoria = item.itens.classificacao_categoria;
        serialized.itens.classificacaoTotal = item.itens.classificacao_total;
        serialized.itens.equipe = item.itens.equipe;
        serialized.itens.velocidadeMedia = item.itens.velocidade_media;
  
        positionsSerialized.push(serialized);
      }
  
      return positionsSerialized;
    }
  
  
  }
