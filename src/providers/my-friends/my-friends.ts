import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { HttpService } from '../../services/http-service';

/*
  Generated class for the MyFriendsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MyFriendsProvider {

  
  constructor(public http: HttpService) {
  }

  getMyFriends(specifications){

    //specifications.userId = "20705300";

    let url = "http://webservices.esferabr.com.br/AppAtivo/getFriends?";
    let params = [];

    specifications.userId   ? params.push("user_id="+specifications.userId) : null;
    specifications.order    ? params.push("order="+specifications.order)    : null;
    specifications.offset   ? params.push("offset="+specifications.offset)  : null;

    url += params.join('&');

    return this.http.get(url)
  }

  addAFriend(specifications){

    let url = "http://webservices.esferabr.com.br/AppAtivo/addFriends?";
    let params = [];
    //specifications.userId = "20705300"

    specifications.userId   ? params.push("user_id="+specifications.userId)         : null;
    specifications.doc      ? params.push("doc="+specifications.doc)                : null;
    specifications.friendId ? params.push("friend_id="+specifications.friendId)     : null;
    specifications.token    ? params.push("token="+specifications.token)            : null;


    url += params.join('&');

    //console.log(url)

    return this.http.post(url, "");
  }

  deleteFriend(specifications){
    
    let url ="http://webservices.esferabr.com.br/AppAtivo/deleteFriends?"
    //remover
    //specifications.token = "czNTIzNTU5NDA0YWQwOGE4YjIzLjU1MjczNzUx"

    let params = [];

    specifications.userId   ? params.push("user_id="+specifications.userId)         : null;
    specifications.friendId ? params.push("friend="+specifications.friendId)        : null;
    specifications.token    ? params.push("token="+specifications.token)            : null;

    url += params.join('&');

    console.log(url)

    return this.http.post(url, "");

  }

}
