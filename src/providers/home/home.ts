import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class HomeProvider {

  private _API_URL = 'http://webservices.esferabr.com.br/Mobile/TipoEvento'

  constructor(public http: Http) {
    console.log('Hello HomeProvider Provider');
  }

  /**
   * List events types to select events in calendar
   * @param
   *
   */
  getEventTypes(){
    return new Promise((resolve, reject) => {
      this.http.get(this._API_URL).subscribe(( data: any ) => {
        console.log( data )
        resolve(data.json())
      },
      (error) => {
        reject(error.json())
      })
    })
  }

}
