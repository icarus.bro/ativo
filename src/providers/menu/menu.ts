import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { Storage } from '@ionic/storage';
/*
  Generated class for the MenuProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MenuProvider {

  loginButton: string = 'Entrar';
  
    constructor() {
    }
  
    check(name){
      if(!name){
        this.loginButton = "Entrar"
      }else{
        this.loginButton = "Sair"
      }
    }

}
