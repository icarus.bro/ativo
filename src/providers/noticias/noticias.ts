import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { CategoriaModel } from '../../models/categoria-model';
import { NoticiasModel, NoticiasEasyLoadModel } from '../../models/noticias-model';

/*
  Generated class for the NoticiasProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class NoticiasProvider {

  prefix = [
    'http://www.atletas.info/apprss/?type=',
    'http://uy.activo.news/apprss/?type=',
    'mx.activo.news/apprss?type=',
    'cl.activo.news/apprss?type=',
    'https://www.ativo.com/apprss?type=',
  ]

  sufixo = [
    "category",
    "post&category=ID&qtd=4",
    "post&post_id=",
    "latest&qtd=4",
  ]


  constructor(public http: Http) {
    console.log();
  }
  
  getNoticiasPorCategoria(pais: string, id: number,quantidade: number){
    switch (pais){
      case "argentina":{
        return this.http.get(this.prefix[0] + 'post&category=' + id + '&qtd=' + quantidade);
      }
      case "uruguay":{
        return this.http.get(this.prefix[1] + 'post&category=' + id + '&qtd=' + quantidade);
      }
      case "mexico":{
        return this.http.get(this.prefix[2] + 'post&category=' + id + '&qtd=' + quantidade);
      }
      case "chile":{
        return this.http.get(this.prefix[3] + 'post&category=' + id + '&qtd=' + quantidade);
      }
      default:{
        return this.http.get(this.prefix[4] + 'post&category=' + id + '&qtd=' + quantidade);
      }
    }
  }

  getNoticiaPorId(pais: string, id: number){
    switch (pais){
      case "argentina":{
        return this.http.get(this.prefix[0] + 'post&post_id=' + id);
      }
      case "uruguay":{
        return this.http.get(this.prefix[1] + 'post&post_id=' + id);
      }
      case "mexico":{
        return this.http.get(this.prefix[2] + 'post&post_id=' + id);
      }
      case "chile":{
        return this.http.get(this.prefix[3] + 'post&post_id=' + id);
      }
      default:{
        return this.http.get(this.prefix[4] + 'post&post_id=' + id);
      }
    }
  }

  getNoticiasPorNacionalidade(pais: string, indice: number){
    switch (pais){
      case "argentina":{
        return this.http.get(this.prefix[0] + this.sufixo[indice]);
      }
      case "uruguay":{
        return this.http.get(this.prefix[1] + this.sufixo[indice]);
      }
      case "mexico":{
        return this.http.get(this.prefix[2] + this.sufixo[indice]);
      }
      case "chile":{
        return this.http.get(this.prefix[3] + this.sufixo[indice]);
      }
      default:{
        return this.http.get(this.prefix[4] + this.sufixo[indice]);
      }
    }
  }

  getNoticiasPorIdEvento(id: number){
    return this.http.get("https://www.ativo.com/wp-admin/admin-ajax.php?action=coberturas&id_evento="+id+"&"+new Date().getHours+new Date().getMinutes+"00");
  }

  populateNoticiasSimples(noticiasBrutas: Array<any>){
    var noticiasSericalizadas = new Array<NoticiasEasyLoadModel>();

    for(let bruta of noticiasBrutas){
      var noticia = new NoticiasEasyLoadModel()

      noticia.imageurl = bruta.imagem;
      noticia.link = bruta.url;
      noticia.content = bruta.resumo;
      noticia.title = bruta.titulo;
      noticia.postId = bruta.id;

      noticiasSericalizadas.push(noticia);
    }

    return noticiasSericalizadas
  }

  populateNoticias(noticiasBrutas: Array<any>){
    var noticiasSericalizadas = new Array<NoticiasModel>();

    for(let bruta of noticiasBrutas){
      var noticia = new NoticiasModel()

      noticia.postId = bruta.post_id;
      noticia.title = this.cleanContainers(bruta.title);
      noticia.link = bruta.link;
      noticia.comments = bruta.comments;
      noticia.excerpt = bruta.excerpt;
      noticia.content = this.cleanContainers(bruta.content).substring(0,160);
      noticia.fullText = this.cleanContainers(bruta.content);
      noticia.date = bruta.date;
      noticia.creator = bruta.creator;
      noticia.imageurl = bruta.imageurl;
      noticia.category = bruta.category;
      noticia.categoryCor = bruta.category_cor;

      noticia.content = this.formatContent(noticia.content);
      noticia.title = this.formatTitle(noticia.title);
      noticiasSericalizadas.push(noticia);

    }
    return noticiasSericalizadas
  }

  populateCatagorias(categoriasBrustas: Array<any>){
    var categoriasSerializadas = new Array<CategoriaModel>();

    for (let bruta of categoriasBrustas){
      var categoria = new CategoriaModel();

      categoria.termId = bruta.term_id;
      categoria.name = bruta.name;
      categoria.slug = bruta.slug;
      categoria.termGroup = bruta.term_group;
      categoria.termTaxonomyId = bruta.term_taxonomy_id;
      categoria.taxonomy = bruta.taxonomy;
      categoria.description = bruta.description;
      categoria.parent = bruta.parent;
      categoria.count = bruta.count;
      categoria.filter = bruta.filter;
      categoria.termOrder = bruta.term_order;
      categoria.cor = bruta.cor;

      categoriasSerializadas.push(categoria);

    }

    return categoriasSerializadas;
  }

  cleanContainers(text){
    var regex = /(&nbsp;|<([^>]+)>)/ig
      ,   body = text
      ,   result = body.replace(regex, "");

     return result;
  }

  formatContent(text: string){
    var index = text.lastIndexOf('.');
    
    if(index != -1 && index > 80){
      return (text.substring(0, index+1)+'..');
    }else{
      var index = text.lastIndexOf('.');
      return text.substring(0, text.lastIndexOf(' ')+1)+'...'
    }
  }

  formatTitle(text: string){
    var valid: boolean = false
    var index: number
    while(!valid){
      var index = text.indexOf('&#')
      if(index == -1){
        valid = true
      }else{
        var remove = text.substring(index, index+7);
        text = text.replace(remove, '');
      }
    }
    return text
  }


}

