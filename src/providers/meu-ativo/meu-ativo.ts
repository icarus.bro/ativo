import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { UserLoginProvider } from '../user-login/user-login';
import { MeusResultadoModel } from '../../models/meus-resultados-model';
import { Foto, FotoModel } from '../../models/foto-model';

/*
  Generated class for the MeuAtivoProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MeuAtivoProvider {


  prefix: string = 'http://webservices.esferabr.com.br/Ativo/';

    constructor(public http: Http,
                private user: UserLoginProvider) {

    }

    getMyResults(){
      // console.log(this.prefix + 'ResultadoPorId?id_usuario='+this.user.user.idUser+"&modo=bb");
      // return this.http.get(this.prefix + 'ResultadoPorId?id_usuario='+this.user.user.idUser+"&modo=bb")
      // return this.http.get(this.prefix + 'ResultadoPorId?id_usuario='+this.user.user.idUser)
      // return this.http.get(`${this.prefix}ResultadoPorId?id_usuario=${this.user.user.idUser}&modo=bb`)
      return this.http.get(`${this.prefix}ResultadoPorId?id_usuario=${this.user.user.idUser}`)
    }

    setMeusResultados(meusResultados: Array<any>){

      var resultados = new Array<MeusResultadoModel>();
      for(let resultado of meusResultados){
        let res = new MeusResultadoModel();

        res.idResultado = resultado.id_resultado;
        res.nome = resultado.nome;
        res.sexo = resultado.sexo;
        res.categoria = resultado.categoria;
        res.equipe = resultado.equipe;
        res.tempoTotal = resultado.tempo_total;
        res.paceMedio = resultado.nr_pace;
        res.certificado = resultado.nm_certificado_evento;
        res.idEvento = resultado.id_evento;
        res.evento = resultado.ds_evento;
        res.numPeito = resultado.num_peito;
        res.dataEvento = new Date(resultado.dt_evento.split(" ")[0]+"T03:00:00");
        res.local = resultado.ds_local;
        res.cidade = resultado.ds_cidade;
        res.estado = resultado.ds_estado;
        res.modalidade = resultado.ds_modalidade;
        res.urlImagem.banner = resultado.ds_url_imagem.img_banner_evento;
        res.urlImagem.logo = resultado.ds_url_imagem.img_logo_evento;
        res.posicao = resultado.itens.classificacao_total;

        // console.log(res)

        resultados.push(res);
      }

      return resultados;
    }

    getMinhasFotos(){
      // return this.http.get(this.prefix + `EventosFotosPorUserID?id_usuario=${this.user.user.idUser}&modo=bb`);
      return this.http.get(this.prefix + `EventosFotosPorUserID?id_usuario=${this.user.user.idUser}`);

      // return this.http.get(this.prefix + 'EventosFotosPorUserID?id_usuario='+'20278046');
    }

    setMinhasFotos(minhasFotos: Array<any>){

      var fotos = new Array<FotoModel>();

      for(let imagem of minhasFotos){
        let img = new FotoModel();

        img.idUsuario = imagem.id_usuario;
        img.idEvento = imagem.id_evento;
        img.ourocard = false;
        // this.ourocardProvider.isPaidWithOurocard(img.idEvento).then( res => {
        //   img.ourocard = res;
        //   // img.ourocard = true;
        // });
        img.nomeEvento = imagem.ds_evento;
        img.dataEvento = imagem.dt_evento;
        img.local = imagem.ds_local;
        img.numeroPeito = imagem.nr_peito;
        img.modalidade = imagem.ds_modalidade;
        img.fotos = this.colecaoFotos(imagem.fotos);

        fotos.push(img)
      }

      return fotos;
    }

    colecaoFotos(imagens){
      var fotos = new Array<Foto>()

      for (let img of imagens){
        var foto = new Foto();

        foto.id = img.id;
        foto.url = img.fotos;

        fotos.push(foto);
      }

      return fotos;
    }





}
