import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { ModalidadeModel } from '../../models/modalidade-model';
import { HttpService } from '../../services/http-service';
import { Util } from '../../util/util';
import { CalendarioEventosModel } from '../../models/caledario-eventos-model';
import { ResultadoEventoModel } from '../../models/resultado-evento-model';

/*
  Generated class for the HomePageProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class HomePageProvider {

  prefix = 'http://webservices.esferabr.com.br/Ativo/';
  private API_URL = 'https://reqres.in/api/';

	constructor(public http: HttpService, private util: Util) { }



  getEventoPorCidade(cidade: string){
    return this.http.get(this.prefix + 'TipoEventoPorLocal?local='+cidade);
  }

  getResultados(cidade: string, offset: number){
    // return this.http.get(this.prefix + 'Evento?modo=resultado&local='+cidade+'&offset='+offset);
    return this.http.get(this.prefix + 'Evento?modo=resultado');
  }

  getResultsByDateRange(specificSearch: any) {
    let url = "http://webservices.esferabr.com.br/Ativo/Evento";
    let parans = [];


    specificSearch.mode          ? parans.push('modo='         + specificSearch.mode)                               : null;
    // specificSearch.local         ? parans.push('local='        + this.util.getClearCityName(specificSearch.local))  : null;
    // specificSearch.offset        ? parans.push('offset='       + specificSearch.offset)                             : 'offset=0';
    // specificSearch.dataInicial   ? parans.push('dt_ini='       + this.covertDate(specificSearch.dataInicial))       : null;
    // specificSearch.dataFinal     ? parans.push('dt_fim='       + this.covertDate(specificSearch.dataFinal))         : null;
    // specificSearch.idModalidade  ? parans.push('modalidade='   + specificSearch.idModalidade)                        : null;
    // specificSearch.internacional ? parans.push('internacional='+ specificSearch.internacional)                      : null;
    // specificSearch.nomeEvento    ? parans.push('pc='           + specificSearch.nomeEvento)                         : null;

    if(parans.length)
      url += '?';

    url += parans.join('&');

    return this.http.get(url)
  }

  private covertDate(date: string) {
    let arr = date.split('/');
    return arr[1] + '/' + arr[0] + '/' + arr[2];
  }

  getEventoPorId(eventoId: number){
    return this.http.get(this.prefix + 'EventoById?id=' +eventoId)
  }

  // Make a new checkin
  doNewCheckin(facebookData: any, eventData: any) {
    return new Promise((resolve, reject) => {

			console.log( facebookData );
			console.log( eventData );

      let url = "http://webservices.esferabr.com.br/Ativo/CheckinBB";
      // let url = "http://webservice/Ativo/CheckinBB";
      let body = [];

      eventData.idUserEsfera          ? body.push('id_usuario='   + eventData.idUserEsfera)      : null;
      facebookData.userFacebookId     ? body.push('id_facebook='  + facebookData.userFacebookId) : null;
      eventData.idEvent               ? body.push('id_evento='     + eventData.idEvent)           : null;
      facebookData.token              ? body.push('token='        + facebookData.token)          : null;

      if(body.length)
        url += '?app=bb&';

      url += body.join('&');
      console.log( body )

      this.http.get(url)
        .subscribe((result: any) => {
          console.log( result )
          resolve(result.json());
        },
        (error) => {
          reject(error.json());
        })
    });
  }

  populateCalendarEvents(calendarioEventos: Array<any>){
    let calendarioEv = new Array<CalendarioEventosModel>();

    for (let ce of calendarioEventos){
      var tipoEvento = new CalendarioEventosModel();

      tipoEvento.idTipoEvento = ce.id_tipo_evento;
      tipoEvento.dsTipoEvento = ce.ds_tipo_evento;
      tipoEvento.contador = ce.contador;
      tipoEvento.thumbTipoEvento = ce.thumb_tipo_evento;
      tipoEvento.iconeTipoEvento = ce.icone_tipo_evento;

      calendarioEv.push(tipoEvento);
    }
    return calendarioEv;
  }

  getADS(){

    const timestamp = new Date().getTime()

    let url = `http://s3-app-circuitobancodobrasil.s3.amazonaws.com/publicidade.json?t=${timestamp}`
    return this.http.get(url)
  }

  populateResultsEvents(resultadosEventos: Array<any>){
    let resultadosEv = new Array<ResultadoEventoModel>();

    for (let re of resultadosEventos){
      var resultadoEvento = new ResultadoEventoModel();

      resultadoEvento.idEvento = re.id_evento;
      resultadoEvento.dsEvento = re.ds_evento;
      resultadoEvento.dsEventoES = re.ds_evento_es;
      resultadoEvento.dsEventoEN = re.ds_evento_en;
      resultadoEvento.dsDescricaoEventoEN = re.ds_descricao_evento_en;
      resultadoEvento.dsDescricaoEventoES = re.ds_descricao_evento_es;
      resultadoEvento.descriptionEn = re.description_en;
      resultadoEvento.descriptionEs = re.description_es;
      resultadoEvento.dsYoutube = re.ds_youtube;
      resultadoEvento.dsDescricaoEventoResumido = re.ds_descricao_evento_resumido;
      resultadoEvento.dtEvento = new Date(re.dt_evento.split(" ")[0]+"T03:00:00");
      resultadoEvento.dsTipoData = re.ds_tipo_data;
      resultadoEvento.idCircuito = re.id_circuito;
      resultadoEvento.dsCircuito = re.ds_circuito;
      resultadoEvento.dtInicioInscricao = new Date(re.dt_inicio_inscricao.split(" ")[0]+"T03:00:00");
      resultadoEvento.dtFimInscricao = new Date(re.dt_fim_inscricao.split(" ")[0]+"T03:00:00");
      resultadoEvento.hrEvento = re.hr_evento;
      resultadoEvento.flCarrossel = re.fl_carrossel;
      resultadoEvento.dsEndereco = re.ds_endereco;
      resultadoEvento.dsBairro = re.ds_bairro;
      resultadoEvento.keywords = re.keywords;
      resultadoEvento.description = re.description;
      resultadoEvento.dsCidade = re.ds_cidade;
      resultadoEvento.idContinente = re.id_continente;
      resultadoEvento.dsContinente = re.ds_continente;
      resultadoEvento.dsPais = re.ds_pais;
      resultadoEvento.dsSite = re.ds_site
      resultadoEvento.dsEstado = re.ds_estado;
      resultadoEvento.dsEstadoNome = re.ds_estado_name;
      resultadoEvento.dsSite = re.ds_site;
      resultadoEvento.dsLocal = re.ds_local;
      resultadoEvento.flEncerrarInscricao = re.fl_encerrar_inscricao;
      resultadoEvento.flResultado = re.fl_resultado;
      resultadoEvento.flHome = re.fl_home;
      resultadoEvento.idTipoEvento = re.id_tipo_evento;
      resultadoEvento.dsTipoEvento = re.ds_tipo_evento;
      resultadoEvento.hashtagInstagram = re.hashtag_instagram;
      resultadoEvento.flEventoDaCasa = re.fl_evento_da_casa;
      resultadoEvento.flFoto = re.fl_foto;
      resultadoEvento.flEventoSimples = re.fl_evento_simples;
      resultadoEvento.flFotoNome = re.fl_foto_nome;
      resultadoEvento.dsInfoGrupo = re.ds_info_grupo;
      resultadoEvento.dataUpdate = re.data_update;
      resultadoEvento.idEventoSituacao = re.id_evento_situacao;
      resultadoEvento.idSituacaoCadastro = re.id_situacao_cadastro;
      resultadoEvento.flViradaPorLote = re.fl_virada_por_lote;
      resultadoEvento.idPaisOrigem = re.id_pais_origem;
      resultadoEvento.horaInicioCorrida = re.hora_inicio_corrida;
      resultadoEvento.horaFimCorrida = re.hora_fim_corrida;
      resultadoEvento.surveyMonkey = re.survey_monkey;
      resultadoEvento.nmCertificadoEvento = re.nm_certificado_evento
      resultadoEvento.idCity = re.id_city;
      resultadoEvento.cityBr = re.city_br;
      resultadoEvento.cityEn = re.city_en;
      resultadoEvento.cityFr = re.city_fr;
      resultadoEvento.cityDe = re.city_de;
      resultadoEvento.cityEs = re.city_es;
      resultadoEvento.countryBr = re.country_br;
      resultadoEvento.countryEn = re.country_en;
      resultadoEvento.countryFr = re.country_fr;
      resultadoEvento.countryDe = re.country_de;
      resultadoEvento.countryEs = re.country_es;
      resultadoEvento.stateBr = re.state_br;
      resultadoEvento.stateEn = re.state_en;
      resultadoEvento.stateFr = re.state_fr;
      resultadoEvento.stateDe = re.state_de;
      resultadoEvento.stateEs = re.state_es;
      resultadoEvento.dsLinkFaq = re.ds_link_faq;
      resultadoEvento.fotoClube = re.foto_clube;
      resultadoEvento.insOpen = re.insOpen;
      resultadoEvento.dsUrlImagem.banner = re.ds_url_imagem ? (re.ds_url_imagem.img_banner_evento || null) : null;
      resultadoEvento.dsUrlImagem.logo = re.ds_url_imagem ? (re.ds_url_imagem.img_logo_evento || null) : null;

      resultadoEvento.modalidades = re.modalidades ? this.getModalidades(re.modalidades) : [];

      resultadosEv.push(resultadoEvento);
    }

    return resultadosEv;
  }

  getModalidades(moda: Array<any>){

    var modalidade = new Array<ModalidadeModel>();

    for (let m of moda){
      var unidade = new ModalidadeModel();

      unidade.id = m.id_modalidade;
      unidade.nome = m.nm_modalidade;

      modalidade.push(unidade);
    }
    return modalidade;
  }

  // getEventsToCheckIn(userId){
  //   let url = "http://webservices.esferabr.com.br/Ativo/CheckUserBB?id_usuario="
  //   console.log(url+userId)
  //   return this.http.get(url+userId)
  // }

  getEventsToCheckIn(idUser) {

		console.log(" ID USER ");
		console.log( idUser );


    return new Promise((resolve, reject) => {
      let url = "http://webservices.esferabr.com.br/Ativo/CheckUserBB?id_usuario=" + idUser
      this.http.get(url)
        .subscribe((result: any) => {
          resolve(result.json());
        },
        (error) => {
          reject(error.json());
        })
    });
  }



  getAll(token, idFacebook, eventId) {
    return new Promise((resolve, reject) => {


			console.log("HOME PAGE PROVIDER");
			console.log(token)
			console.log(idFacebook)
			console.log(eventId)


			let url = 'https://webservices.esferabr.com.br/Ativo/FriendsBB?id_facebook=' + idFacebook + '&&id_evento=' + eventId + '&&token=' + token

			console.log( "URL" )
			console.log( url )

      this.http.get(url)
        .subscribe((result: any) => {
					console.log( result );
          resolve(result.json());
        },
        (error) => {
          reject(error.json());
        });
    });
  }
}

