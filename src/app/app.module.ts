import { ResultPositionRaceItemComponent } from './../components/result-position-race-item/result-position-race-item';
import { ResultDetailComponent } from './../components/result-detail/result-detail';
import { ResultsEventsItemComponent } from './../components/results-events-item/results-events-item';
import { EventModalDateFilterPage } from './../pages/event-modal-date-filter/event-modal-date-filter';
import { ResultAthleteDetailsPage } from './../pages/result-athlete-details/result-athlete-details';
import { EventRankingPage } from './../pages/event-ranking/event-ranking';
import { AdvancedFilterPage } from './../pages/advanced-filter/advanced-filter';
import { GeolocationComponentFilterComponent } from './../components/geolocation-component-filter/geolocation-component-filter';
import { EventModalGeolocationCategoryPage } from './../pages/event-modal-geolocation-category/event-modal-geolocation-category';
import { UserPurchaseModalKitPage } from './../pages/user-purchase-modal-kit/user-purchase-modal-kit';
import { EventsMyResultListPage } from './../pages/events-my-result-list/events-my-result-list';
import { EditorialComponentItemComponent } from './../components/editorial-component-item/editorial-component-item';
import { EventResultDetailPage } from './../pages/event-result-detail/event-result-detail';
import { ActionSheet } from '@ionic-native/action-sheet';
import { SocialSharing } from '@ionic-native/social-sharing';
import { EventModalRoutePage } from './../pages/event-modal-route/event-modal-route';
import { EventModalRegulamentPage } from './../pages/event-modal-regulament/event-modal-regulament';
import { EventModalKitPage } from './../pages/event-modal-kit/event-modal-kit';
import { TipsFullListPage } from './../pages/tips-full-list/tips-full-list';
import { UserPreferencesConfigPage } from './../pages/user-preferences-config/user-preferences-config';
import { ResultTrendPage } from './../pages/result-trend/result-trend';
import { ResultResultPage } from './../pages/result-result/result-result';
import { ResultPhotoPage } from './../pages/result-photo/result-photo';
import { ResultInfoPage } from './../pages/result-info/result-info';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder } from '@ionic-native/native-geocoder';
import { LocationAccuracy } from '@ionic-native/location-accuracy';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginPage } from '../pages/login/login';

import { TabsMainPage } from '../pages/tabs-main/tabs-main';
import { TabsResultsPage } from './../pages/tabs-results/tabs-results';
import { TabsNewsTipsPage } from '../pages/tabs-news-tips/tabs-news-tips';
import { TabsSocialCompanyPage } from '../pages/tabs-social-company/tabs-social-company';
import { TabsUserPage } from '../pages/tabs-user/tabs-user';

import { EventsFullCalendarPage } from '../pages/events-full-calendar/events-full-calendar';
import { EventsTrendsCalendarPage } from '../pages/events-trends-calendar/events-trends-calendar';
import { EventsResultListPage } from './../pages/events-result-list/events-result-list';
import { EventsUserListPage } from './../pages/events-user-list/events-user-list';
import { EventsFullListCalendarPage } from './../pages/events-full-list-calendar/events-full-list-calendar';
import { EventSelectedDetailsPage } from './../pages/event-selected-details/event-selected-details';

import { InfoTipsPage } from '../pages/info-tips/info-tips';
import { InfoNewsPage } from '../pages/info-news/info-news';

import { FeedFacebookPage } from '../pages/feed-facebook/feed-facebook';
import { FeedInstagramPage } from '../pages/feed-instagram/feed-instagram';
import { FeedSpotifyPlaylistsPage } from '../pages/feed-spotify-playlists/feed-spotify-playlists';
import { FeedPhrasesPage } from '../pages/feed-phrases/feed-phrases';
// COMPONENTS
import { EventComponentItemComponent } from './../components/event-component-item/event-component-item';
import { TipComponentItemComponent } from './../components/tip-component-item/tip-component-item';
import { UserComponentPurchaseItemComponent } from './../components/user-component-purchase-item/user-component-purchase-item';
import { CategoryComponentItemComponent } from './../components/category-component-item/category-component-item';
// import { SuperTabsModule, SuperTabsController } from 'ionic2-super-tabs';
import { UserSignUpPage } from '../pages/user-sign-up/user-sign-up';
import { UserForgotPasswordPage } from '../pages/user-forgot-password/user-forgot-password';
import { UserPhotosPage } from '../pages/user-photos/user-photos';
import { UserPurchasesPage } from '../pages/user-purchases/user-purchases';
import { UserProfilePage } from '../pages/user-profile/user-profile';
import { UserMyResultsPage } from '../pages/user-my-results/user-my-results';
import { UserPurchaseDetailPage } from './../pages/user-purchase-detail/user-purchase-detail';

import { AppAvailability } from '@ionic-native/app-availability';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { ParalaxHeaderDirective } from '../directives/paralax-header/paralax-header';

import { CallNumber } from '@ionic-native/call-number';
import { Calendar } from '@ionic-native/calendar';
import { InfoNewsReadPage } from '../pages/info-news-read/info-news-read';
import { InfoTipsCategorySelectedPage } from '../pages/info-tips-category-selected/info-tips-category-selected';
import { InfoTipsReadPage } from '../pages/info-tips-read/info-tips-read';
import { UserLoginProvider } from '../providers/user-login/user-login';
import { MenuProvider } from '../providers/menu/menu';
import { HttpService } from '../services/http-service';
import { NetworkService } from '../services/network-service';
import { HttpModule } from '@angular/http';

import { IonicStorageModule } from '@ionic/storage';
import { CalendarProvider } from '../providers/calendar/calendar';
import { NotifyComponent } from '../components/notify/notify';
import { Util } from '../util/util';
import { Localization } from '../util/localization';
import { EventItem } from '../components/event-item/event-item';
import { MeusPedidosProvider } from '../providers/meus-pedidos/meus-pedidos';
import { KitWithdrawProvider } from '../providers/kit-withdraw/kit-withdraw';
import { ReciboPage } from '../pages/recibo/recibo';
import { ReceiptAthleteItemComponent } from '../components/receipt-athlete-item/receipt-athlete-item';
import { ReceiptEventItemComponent } from '../components/receipt-event-item/receipt-event-item';
import { CapitalizePipe } from '../pipes/capitalize/capitalize';
import { ReceiptProductItemComponent } from '../components/receipt-product-item/receipt-product-item';
import { RetiradaDeKitPage } from '../pages/retirada-de-kit/retirada-de-kit';
import { KitItemListComponent } from '../components/kit-item-list/kit-item-list';
import { HomePageProvider } from '../providers/home-page/home-page';
import { SingleEventPage } from '../pages/single-event/single-event';
import { WebModelPage } from '../pages/web-model/web-model';
import { KitToBuyListComponent } from '../components/kit-to-buy-list/kit-to-buy-list';
import { MyFriendsProvider } from '../providers/my-friends/my-friends';
import { MyFriendItemListComponent } from '../components/my-friend-item-list/my-friend-item-list';
import { MeusAmigosPage } from '../pages/meus-amigos/meus-amigos';
import { MeuAtivoProvider } from '../providers/meu-ativo/meu-ativo';
import { ResultsProvider } from '../providers/results/results';
import { ResultsPage } from '../pages/results/results';
import { ResultadoAtletaDetalhesPage } from '../pages/resultado-atleta-detalhes/resultado-atleta-detalhes';
import { NoticiasProvider } from '../providers/noticias/noticias';
import { DicasPage } from '../pages/dicas/dicas';
import { NoticiaDetalhePage } from '../pages/noticia-detalhe/noticia-detalhe';
import { FileTransfer } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { HomeProvider } from '../providers/home/home';
import { FilterComponent } from '../components/filter/filter';
import { FilterGeolocationComponent } from '../components/filter-geolocation/filter-geolocation';




@NgModule({
  declarations: [
    MyApp,

    /* PAGES */
    LoginPage,
    UserSignUpPage,
    UserForgotPasswordPage,

    TabsMainPage,
    TabsNewsTipsPage,
    TabsSocialCompanyPage,
    TabsUserPage,
    TabsResultsPage,

    EventsFullCalendarPage,
    EventsTrendsCalendarPage,
    EventSelectedDetailsPage,
    EventsResultListPage,
    EventsFullListCalendarPage,
    EventsUserListPage,
    EventModalKitPage,
    EventModalRegulamentPage,
    EventModalRoutePage,
    EventResultDetailPage,
    EventsMyResultListPage,
    EventModalGeolocationCategoryPage,
    EventRankingPage,
    EventModalDateFilterPage,

    // Pages tab
    InfoNewsPage,
    InfoNewsReadPage,
    InfoTipsPage,
    InfoTipsCategorySelectedPage,
    InfoTipsReadPage,
    ResultInfoPage,
    ResultPhotoPage,
    ResultResultPage,
    ResultTrendPage,
    ResultAthleteDetailsPage,

    FeedFacebookPage,
    FeedInstagramPage,
    FeedSpotifyPlaylistsPage,
    FeedPhrasesPage,

    UserProfilePage,
    UserMyResultsPage,
    UserPhotosPage,
    UserPurchasesPage,
    UserPurchaseDetailPage,
    UserPreferencesConfigPage,
    UserPurchaseModalKitPage,

    TipsFullListPage,

    HomePage,
    ListPage,
    AdvancedFilterPage,

    /* COMPONENTS */
    CategoryComponentItemComponent,
    EventComponentItemComponent,
    ParalaxHeaderDirective,
    TipComponentItemComponent,
    UserComponentPurchaseItemComponent,
    EditorialComponentItemComponent,
    NotifyComponent,
    EventItem,
    ReceiptAthleteItemComponent,
    ReceiptEventItemComponent,
    ReceiptProductItemComponent,
    KitItemListComponent,
    KitToBuyListComponent,
    GeolocationComponentFilterComponent,

    MyFriendItemListComponent,
    ResultsEventsItemComponent,
    ResultDetailComponent,
    ResultPositionRaceItemComponent,

    ReciboPage,
    CapitalizePipe,
    RetiradaDeKitPage,
    SingleEventPage,
    WebModelPage,

    MeusAmigosPage,
    ResultsPage,
    ResultadoAtletaDetalhesPage,
    DicasPage,
    NoticiaDetalhePage,

    FilterComponent,
    FilterGeolocationComponent,

  ],
  imports: [
    BrowserModule,

    // SuperTabsModule.forRoot(),
    IonicModule.forRoot(MyApp, {
      tabsPlacement: 'top',
      tabsHideOnSubPages: true,
      mode: 'md'
    },),
    HttpModule,
    IonicStorageModule.forRoot()
  ],

  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,

    /* PAGES */
    LoginPage,
    UserSignUpPage,
    UserForgotPasswordPage,

    TabsMainPage,
    TabsNewsTipsPage,
    TabsSocialCompanyPage,
    TabsUserPage,
    TabsResultsPage,

    EventsFullCalendarPage,
    EventsTrendsCalendarPage,
    EventsFullListCalendarPage,
    EventSelectedDetailsPage,
    EventsResultListPage,
    EventsUserListPage,
    EventModalKitPage,
    EventModalRegulamentPage,
    EventModalRoutePage,
    EventResultDetailPage,
    EventsMyResultListPage,
    EventModalGeolocationCategoryPage,
    EventRankingPage,
    EventModalDateFilterPage,

    // Pages tab
    InfoNewsPage,
    InfoNewsReadPage,
    InfoTipsPage,
    InfoTipsCategorySelectedPage,
    InfoTipsReadPage,
    ResultInfoPage,
    ResultPhotoPage,
    ResultResultPage,
    ResultTrendPage,
    ResultAthleteDetailsPage,

    FeedFacebookPage,
    FeedInstagramPage,
    FeedSpotifyPlaylistsPage,
    FeedPhrasesPage,

    UserProfilePage,
    UserMyResultsPage,
    UserPhotosPage,
    UserPurchasesPage,
    UserPurchaseDetailPage,
    UserPreferencesConfigPage,
    UserPurchaseModalKitPage,

    TipsFullListPage,

    HomePage,
    ListPage,
    AdvancedFilterPage,

    /* COMPONENTS */
    CategoryComponentItemComponent,
    EventComponentItemComponent,
    TipComponentItemComponent,
    UserComponentPurchaseItemComponent,
    EditorialComponentItemComponent,
    NotifyComponent,
    EventItem,
    ReceiptAthleteItemComponent,
    ReceiptEventItemComponent,
    ReceiptProductItemComponent,
    KitItemListComponent,
    KitToBuyListComponent,

    GeolocationComponentFilterComponent,

    MyFriendItemListComponent,
    ResultsEventsItemComponent,
    ResultDetailComponent,
    ResultPositionRaceItemComponent,


    ReciboPage,
    RetiradaDeKitPage,
    SingleEventPage,
    WebModelPage,

    MeusAmigosPage,
    ResultsPage,
    ResultadoAtletaDetalhesPage,
    DicasPage,
    NoticiaDetalhePage,

    FilterComponent,
    FilterGeolocationComponent,


  ],
  providers: [
    Storage,
    StatusBar,
    SplashScreen,
    AppAvailability,
    InAppBrowser,
    CallNumber,
    Calendar,
    ActionSheet,
    SocialSharing,
    Geolocation,
    NativeGeocoder,
    LocationAccuracy,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    UserLoginProvider,
    MenuProvider,


    /* CODE - Banco do Brasil */
    /* Services */
    HttpService,
    NetworkService,
    MenuProvider,
    UserLoginProvider,
    HttpService,
    CalendarProvider,
    Util,
    Localization,
    MeusPedidosProvider,
    KitWithdrawProvider,
    MeusPedidosProvider,
    HomePageProvider,
    MyFriendsProvider,
    MeuAtivoProvider,
    ResultsProvider,
    NoticiasProvider,
    FileTransfer,
    File,
    HomeProvider
    // File,


  ]
})
export class AppModule {}
