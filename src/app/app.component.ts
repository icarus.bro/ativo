import { EventsFullListCalendarPage } from './../pages/events-full-list-calendar/events-full-list-calendar';
import { EventsMyResultListPage } from './../pages/events-my-result-list/events-my-result-list';
import { InfoNewsPage } from './../pages/info-news/info-news';

import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { LoginPage } from '../pages/login/login';
import { TabsMainPage } from '../pages/tabs-main/tabs-main';
import { TabsNewsTipsPage } from '../pages/tabs-news-tips/tabs-news-tips';
import { TabsSocialCompanyPage } from '../pages/tabs-social-company/tabs-social-company';
import { TabsUserPage } from '../pages/tabs-user/tabs-user';
import { UserPurchasesPage } from '../pages/user-purchases/user-purchases';
import { UserPreferencesConfigPage } from './../pages/user-preferences-config/user-preferences-config';
import { UserLoginProvider } from '../providers/user-login/user-login';
import { MenuProvider } from '../providers/menu/menu';
import { Storage } from '@ionic/storage';
import { RetiradaDeKitPage } from '../pages/retirada-de-kit/retirada-de-kit';
import { MeusAmigosPage } from '../pages/meus-amigos/meus-amigos';
import { SingleEventPage } from '../pages/single-event/single-event';

// import { Storage } from '@ionic/storage';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  userLogged: any
  rootPage: any = TabsMainPage
  // rootPage: any = EventsFullListCalendarPage

  pages: Array<{title: string, component: any, icon: string}>;

  //CODE - Banco do Brasil
  public login: string;

  constructor(public platform: Platform,
              public statusBar: StatusBar,
              public splashScreen: SplashScreen,
              //CODE - Banco do Brasil
              private user: UserLoginProvider,
              // private alertCtrl: AlertController,
              public menuProvider: MenuProvider,
              private storage: Storage,
              private event: Events,
            ) {



    this.initializeApp();

     // used for an example of ngFor and navigation
     this.pages = [
      { title: 'Ativo.com', icon:'home', component: TabsMainPage },
      { title: 'Meus Resultados', icon:'stats', component: EventsMyResultListPage },
      { title: 'Minhas fotos', icon:'camera', component: EventsMyResultListPage },
      { title: 'Meus Pedidos', icon:'cart', component: UserPurchasesPage },
      { title: 'Retirada de Kit', icon:'basket', component: RetiradaDeKitPage },
      { title: 'Editar meus dados', icon:'cog', component: TabsMainPage },
    ];

    this.login = this.menuProvider.loginButton;

    //CODE - Banco do Brasil
    this.event.subscribe('changeLoginButton', (user) => {
      this.menuProvider.check(user);
    });

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      //  this.storage.get('user')
      //  this.userLogged = this.storage.get('user')
        this.storage.get('user').then((val) => {
          console.log('Your age is', val)
          this.userLogged = val['name']
          console.log('User Logged', this.userLogged)
        });
    });
  }

  // openPage(page) {
  //   // Reset the content nav to have just this page
  //   // we wouldn't want the back button to show in this scenario
  //   this.nav.setRoot(page.component);
  // }

  //CODE - Banco do Brasil
  goToLogin(){
    if(this.user.user.name == ''){
      this.menuProvider.check(this.user.user.name);
      this.nav.push(LoginPage, TabsMainPage);
    }else{
      this.user.cleanUser();
      this.menuProvider.check(this.user.user.name);
      this.nav.setRoot(TabsMainPage);
    }
  }

  openPage(page) {
    if(this.user.user.name != '' || page.component == TabsMainPage){
      if(page.component != null && page.component != this.nav.last().component){
        this.nav.push(page.component);
      }else if(page.component == this.nav.last().component){
        return
      }else{
        this.nav.setRoot(TabsMainPage);
      }
    }else{
      this.nav.push(LoginPage, page.component);
    }
  }

  goToHome(){
    this.nav.setRoot(TabsMainPage)
  }

  launch(url) {
    this.platform.ready().then(() => {
      open(url, "_blank", "location=no");
    });
  }




}
