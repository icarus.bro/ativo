import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { CallNumber } from '@ionic-native/call-number';
import { Calendar } from '@ionic-native/calendar';

@Component({
  selector: 'page-user-profile',
  templateUrl: 'user-profile.html',
})
export class UserProfilePage {

  user;
  name;
  sex;
  cpf;
  birth;
  adress;
  neigh;
  city;
  state;
  mail;
  cellphone;
  residential_phone;
  emergencial_contact;
  cellphone_em;
  residential_phone_em;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private callNumber: CallNumber,
    private calendar: Calendar) {

      this.name = "Diógenes"
      this.sex = "Masculino"
      this.cpf = "123.456.789-00"
      this.birth = "31/01/1993"
      this.adress = "Rua Dev, 01"
      this.neigh = "Jd. Dev"
      this.city = "São Paulo"
      this.state = "SP"
      this.mail = "dev@dev.com"
      this.cellphone = "(11) 91234-5678"
      this.residential_phone = "(11) 1234-5678"
      this.emergencial_contact = "Dev"
      this.cellphone_em = "(11) 91234-5678"
      this.residential_phone_em =  "(11) 1234-5678"

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserProfilePage');
  }

  callPhone(){
    this.callNumber.callNumber("982521162", true)
    .then(() => console.log('Inicializado com Sucesso!'))
    .catch(() => console.log('Falha'));
  }

  calendarEvent(){
    this.calendar.createEventInteractively('MyCalendar').then(
      (msg) => { this.createEventWithOptions() },
      (err) => { console.log(err); }
    );
  }

  createEventWithOptions(){
    return this.calendar.createEventInteractively("event title", "local", "obs",  new Date(2017, 10, 15, 18, 30, 0, 0), new Date(2017, 100, 17, 12, 0, 0, 0));
  }



}
