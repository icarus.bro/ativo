import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, Events, ToastController, MenuController } from 'ionic-angular';
import { MyFriendsProvider } from '../../providers/my-friends/my-friends';
import { UserLoginProvider } from '../../providers/user-login/user-login';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';

/**
 * Generated class for the MeusAmigosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-meus-amigos',
  templateUrl: 'meus-amigos.html',
})
export class MeusAmigosPage implements OnDestroy {

  myFriends: Array<any>;

    // addFriend = AddFriendPage;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                private myFriendsProvider: MyFriendsProvider,
                private user: UserLoginProvider,
                public loadingCtrl: LoadingController,
                public events: Events,
                private alertCtrl: AlertController,
                private toastCtrl: ToastController,
                private menuCtrl: MenuController) {

      this.myFriends = new Array<any>();

      this.events.subscribe("attfriend", (att)=>{
        let alert = this.alertCtrl.create({
          title: 'Excluir Amigo',
          message: 'Você tem certeza que deseja remover o vínculo com esse usuário?',
          cssClass: 'alert-friend',
          buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'Excluir',
            cssClass: 'btn-delete-friend' ,
            handler: () => {
              let params = {userId: this.user.user.idUser, friendId: att, token: this.user.user.token}

              this.deleteMyFriend(params)

            }
          }
        ]
      });
    alert.present();
      })
    }

    ionViewWillEnter(){
      this.getMyFriends();
    }

    deleteMyFriend(params){
      let loading = this.loadingCtrl.create({
        spinner: 'dots',
        content: 'Deletando da sua lista de Amigos!'
      });
      loading.present();
      this.myFriendsProvider.deleteFriend(params)
        .subscribe(data=>{
          loading.dismiss();
          let response = (JSON.parse(data["_body"]));
          console.log(response);
          if(response.length){
            if(response[0].message.search("removido da sua lista de amigos") > 0){
            let toast = this.toastCtrl.create({
                message: response[0].message,
                duration: 3000,
                position: 'top'
            });
            toast.present();
            console.log(response[0].message)
          }else{
             let alert = this.alertCtrl.create({
                title: 'Erro ao deletar amigo!',
                message: 'Talvez esse atleta já não seja mais seu amigo!',
                cssClass: 'alert-friend',
                buttons: ['Ok']
            });
              alert.present();
            }
          }else{
            if(response.message.search("removido da sua lista de amigos") > 0){
              let toast = this.toastCtrl.create({
                  message: response.message,
                  duration: 3000,
                  position: 'top'
              });
              toast.present();
              console.log(response[0].message)
            }else{
              let alert = this.alertCtrl.create({
                  title: 'Erro ao deletar amigo!',
                  message: response.message,
                  cssClass: 'alert-friend',
                  buttons: ['Ok']
              });
              alert.present();
            }
          }



        }, Error=>{
          console.log("deu ruim para excluir")
          loading.dismiss();
        }, ()=>{
          this.getMyFriends();
        })
    }
    getMyFriends(){
      let loading = this.loadingCtrl.create({
        spinner: 'dots',
        content: 'Recepcionando seus amigos!'
      });
      loading.present();
      this.myFriendsProvider.getMyFriends({userId: this.user.user.idUser, offset: this.myFriends.length})
        .subscribe(data=>{
          console.log("####");
          console.log( typeof data );
          console.log( data );

          this.myFriends = [];
          this.myFriends = JSON.parse(data["_body"]);
          console.log("*****");

          console.log(this.myFriends);
        }, error=>{
          console.log(error);
        }, ()=>{
          loading.dismiss();
        })
    }

    menuToggle(){
      this.menuCtrl.toggle();
    }

    ngOnDestroy() {
      this.events.unsubscribe("attfriend")
    }

  }

