import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MeusAmigosPage } from './meus-amigos';

@NgModule({
  declarations: [
    MeusAmigosPage,
  ],
  imports: [
    IonicPageModule.forChild(MeusAmigosPage),
  ],
})
export class MeusAmigosPageModule {}
