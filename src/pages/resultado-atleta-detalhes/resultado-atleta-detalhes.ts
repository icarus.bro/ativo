import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { AtletaPosicaoModel } from '../../models/atleta-posicao-model';
import { ResultadoEventoModel } from '../../models/resultado-evento-model';

@IonicPage()
@Component({
  selector: 'page-resultado-atleta-detalhes',
  templateUrl: 'resultado-atleta-detalhes.html',
})
export class ResultadoAtletaDetalhesPage {

  public athlete = new AtletaPosicaoModel();
  public event = new ResultadoEventoModel();
  // public minhasFotosPage = MinhasFotosPage;

  constructor(public platform: Platform, public navCtrl: NavController, public navParams: NavParams) {
    this.athlete = this.navParams.get("item");
    this.event = this.navParams.get("evento");
  }

  public certify(){
    this.launch("http://download.myrun.com.br/ferramentas/certificado/?NPeito="+this.athlete.numPeito+"&EventID="+this.athlete.idEvento+"&Template="+this.event.nmCertificadoEvento+"&NProva="+this.event.dsEvento+"&NCidade="+this.event.dsLocal+"&Mod="+this.athlete.modalidade+"&CTotal="+this.athlete.tempoTotal+"&TBruto="+this.athlete.tempoBruto+"&PMedio="+this.athlete.pace+"&VMTotal="+this.athlete.itens.velocidadeMedia+"&TFinal="+this.athlete.tempoTotal+"&Per="+this.athlete.percurso+"&NAME="+this.athlete.nome)
  }

  ionViewDidLoad() {
  }

  public launch(url) {
      this.platform.ready().then(() => {
          open(url, "_blank", "location=no");
      });
  }

}

