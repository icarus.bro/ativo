import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ResultadoAtletaDetalhesPage } from './resultado-atleta-detalhes';

@NgModule({
  declarations: [
    ResultadoAtletaDetalhesPage,
  ],
  imports: [
    IonicPageModule.forChild(ResultadoAtletaDetalhesPage),
  ],
})
export class ResultadoAtletaDetalhesPageModule {}
