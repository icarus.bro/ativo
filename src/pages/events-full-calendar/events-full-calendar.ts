import { HomeProvider } from './../../providers/home/home';
import { Util } from './../../util/util';
import { Localization } from './../../util/localization';

import { EventModalGeolocationCategoryPage } from './../event-modal-geolocation-category/event-modal-geolocation-category';
import { EventsFullListCalendarPage } from './../events-full-list-calendar/events-full-list-calendar';
import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ModalController, ToastController } from 'ionic-angular';

import { Geolocation, Geoposition } from '@ionic-native/geolocation';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult } from '@ionic-native/native-geocoder';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Platform } from 'ionic-angular/platform/platform';

@Component({
  selector: 'page-events-full-calendar',
  templateUrl: 'events-full-calendar.html',
})
export class EventsFullCalendarPage implements OnInit {

  eventTypes: any[]
  currentLocate: any
  categories
  page = EventsFullListCalendarPage
  modalGeolocationFilterPage = EventModalGeolocationCategoryPage
  currentGeolocation: any

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public modalCtrl: ModalController,
              public geolocation: Geolocation,
              public geocoder: NativeGeocoder,
              public toaster: ToastController,
              public platform: Platform,
              public locacationAccuracy: LocationAccuracy,
              private util: Util,
              private localization: Localization,
              private homeProvider: HomeProvider) {

                // this.currentGeolocation
                // console.log( 'CONTRUCT full calendar' )
                // console.log( this.currentGeolocation )


  }

  ngOnInit() {
    this.getCurrentGeolocation()
    this.loadEventTypes()
  }

  /**
   * To search in list
   * @param ev
   */
  getItems(ev: any) {
      // set val to the value of the searchbar
      let val = ev.target.value
      // if the value is an empty string don't filter the items
      if (val && val.trim() != '') {
        this.categories = this.categories.filter((category) => {
          return (category.name.toLowerCase().indexOf(val.toLowerCase()) > -1)
        })
      } else {
        // response to empty string
      }
    }

    /**
     * Load event types(category) to select.
     * @param
     *
     */
    loadEventTypes() {
      this.homeProvider.getEventTypes().then((result: any) => {
        this.eventTypes = result
      }).catch((error: any) => {
        console.log( "Erro ao carregar os tipos de eventos." )
      })
    }

    /**
     * Get current geolocation to filter events
     * @param
     */
    getCurrentGeolocation() {
      this.localization.getCurrentCoordinates().then((response: NativeGeocoderReverseResult) => {
        this.currentGeolocation = response.administrativeArea
        console.log( response.administrativeArea )
      }).catch((error) => {
        console.log( 'Não foi possível capturar a localização.' + error)
      })
    }

    /**
     * Modal to change filter
     * events
     */
    openFilterGeolocation() {
      let modal = this.modalCtrl.create(this.modalGeolocationFilterPage)
      modal.present()
    }
}
