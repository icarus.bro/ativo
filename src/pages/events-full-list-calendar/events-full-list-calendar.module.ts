import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EventsFullListCalendarPage } from './events-full-list-calendar';

@NgModule({
  declarations: [
    EventsFullListCalendarPage,
  ],
  imports: [
    IonicPageModule.forChild(EventsFullListCalendarPage),
  ],
})
export class EventsFullListCalendarPageModule {}
