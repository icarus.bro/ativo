import { NativeGeocoder, NativeGeocoderReverseResult } from '@ionic-native/native-geocoder';
import { Localization } from './../../util/localization';
import { EventModalDateFilterPage } from './../event-modal-date-filter/event-modal-date-filter';
import { AdvancedFilterPage } from './../advanced-filter/advanced-filter';
import { EventModalGeolocationCategoryPage } from './../event-modal-geolocation-category/event-modal-geolocation-category';
import { UserMyResultsPage } from './../user-my-results/user-my-results';
import { EventSelectedDetailsPage } from './../event-selected-details/event-selected-details';
import { Component, ViewChild, Input } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController } from 'ionic-angular';
import { CalendarProvider } from '../../providers/calendar/calendar';
import { Util } from '../../util/util';
import { NetworkService } from '../../services/network-service';
import { NotifyComponent, Types } from '../../components/notify/notify';

import { EventEmitter, OnDestroy, OnInit } from '@angular/core';
import { Events, MenuController } from 'ionic-angular';
import { SingleEventPage } from '../single-event/single-event';

@IonicPage()
@Component({
  selector: 'page-events-full-list-calendar',
  templateUrl: 'events-full-list-calendar.html',
})
export class EventsFullListCalendarPage {

  @ViewChild('notify')
  notify: NotifyComponent;

  public eventCity: string
  eventCount: number
  eventTypeId: number
  eventName : any
  segmentedSelected = 'open-subscriptions'
  // page = EventSelectedDetailsPage
  advancedFilterPage = AdvancedFilterPage
  modalGeolocationFilterPage = EventModalGeolocationCategoryPage
  modalDateFilterPage = EventModalDateFilterPage
  //CODE - Banco do Brasil
  events = [];
  // page = SingleEventPage;
  page = SingleEventPage;
  eventTypeName: string
  currentGeolocation: any

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public alertCtrl: AlertController,
              public modalCtrl: ModalController,
              //CODE - Banco do Brasil
              private calendarProvider: CalendarProvider,
              private util: Util,
              private localization: Localization,
              public networkService: NetworkService,
              public geocoder: NativeGeocoder,
            ) {

  }

  ionViewDidLoad(){


    this.eventTypeName = this.navParams.get('name')
    this.onLoadEvents()
    this.getCurrentGeolocation()
    this.onLoadEventsByGeolocation()


  }

  /**
     * Get current geolocation to filter events
     * @param
     */
    getCurrentGeolocation() {
      this.localization.getCurrentCoordinates().then((response: NativeGeocoderReverseResult) => {
        this.currentGeolocation = response.administrativeArea
        console.log( response.administrativeArea )
      }).catch((error) => {
        console.log( 'Não foi possível capturar a localização.' + error)
      })
    }

  /**
   * Search events by name. Used on input search.
   * @param clear
   */
  public onLoadEvents(clear?: boolean) {

    this.eventTypeId = this.navParams.get('id')
    this.eventCity = this.navParams.get('geolocation')
    this.eventCount = 10

    this.calendarProvider.getEventsByDateRange(this.eventCount, this.eventTypeId, this.eventCity)
      .then((res) => {

        let e = [];
        e = res.json();
        // e.forEach(element =>{
        //   element.dt_evento = new Date(element.dt_evento.split(" ")[0]+"T03:00:00")
        //   element.dt_inicio_inscricao = new Date(element.dt_inicio_inscricao.split(" ")[0]+"T03:00:00")
        //   element.dt_fim_inscricao = new Date(element.dt_fim_inscricao.split(" ")[0]+"T03:00:00")
        // })

      this.events = this.events.concat(e);

      console.log(this.events);
        this.notify.clear();


      }).catch((err) => {

        this.notify.clear();
        this.notify.setNotify({
          type: Types.WARNING,
          message: 'Não foi possível carregar'
        });

      })
  }

  /**
   * Get events by geolocation
   * @param string gelocation
   */
  onLoadEventsByGeolocation() {

      let currentGeolocation = this.currentGeolocation

      this.calendarProvider.getEventsByGeolocation(currentGeolocation).then((response: any) => {
        console.log( "Eventos por geolocalização " )
        // console.log( response['_body'] )
        console.log( response['_body'] )
      })
      .catch((error) => {
        console.log( "Error to seach by geolocation." + error )
      })
  }

  /**
   * Search bar by name. Searchbar.
   * @param
   */
  public onLoadEventsByName(clear?: boolean) {

    this.events = []
    this.calendarProvider.getEventsByName(this.eventName)
    .then((res) => {

      let e = [];
      e = res.json();
      e.forEach(element =>{
        element.dt_evento = new Date(element.dt_evento.split(" ")[0]+"T03:00:00")
        element.dt_inicio_inscricao = new Date(element.dt_inicio_inscricao.split(" ")[0]+"T03:00:00")
        element.dt_fim_inscricao = new Date(element.dt_fim_inscricao.split(" ")[0]+"T03:00:00")
      })

    this.events = this.events.concat(e);
    console.log(this.events);
      this.notify.clear();

    })
    .catch((err) => {

      this.notify.clear();
      this.notify.setNotify({
        type: Types.WARNING,
        message: 'Não foi possível carregar'
      });

    })
  }

  loadedCountryList;

  initializeItems(): void {
    this.events = this.events;
  }


  /**
   * Exec search in list
   * @param ev
   */
  getItems(ev: any) {
    this.initializeItems()

    let val = ev.target.value

    if (val && val.trim() != '') {
      this.events = this.events.filter((event) => {
        return (event.ds_evento.toLowerCase().indexOf(val.toLowerCase()) > -1)
      })
    } else if (val && val.trim() != ''){
      //Nesse else tratar se for um INT com o ID do evento para teste
      this.events = this.events.filter((event) => {
        return (event.id_evento.toLowerCase().indexOf(val.toLowerCase()) > -1)
      })
    } else {
      //Tratar o erro aqui
    }
  }

  /**
   * Open modal to select a new locate
   * @param locate
   *
   */
  openFilterByGeolocation(){
    let searchLocate = this.modalCtrl.create(this.modalGeolocationFilterPage, {
      geolocation: this.currentGeolocation
    })
    searchLocate.present()
  }

  /**
   * Open modal to select range date to filter
   * @param startDate
   * @param endDate
   *
   */
  openFilterByDate() {
    let selectedDates = this.modalCtrl.create(this.modalDateFilterPage)
    selectedDates.present()
  }


}
