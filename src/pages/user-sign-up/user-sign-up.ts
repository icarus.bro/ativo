import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

/**
 * Generated class for the UserSignUpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
selector: 'page-user-sign-up',
templateUrl: 'user-sign-up.html',
})
export class UserSignUpPage {

signUpForm: FormGroup;

  constructor(
    public formBuilder: FormBuilder,
    public navCtrl: NavController, 
    public navParams: NavParams) {

      this.signUpForm = this.formBuilder.group({
        name: ['',[Validators.required, Validators.minLength(3)]]
        //acrescentar os demais
      });
  }
  
  onSubmit(): void{
    console.log(this.signUpForm.value);
    
  }




  ionViewDidLoad() {
    console.log('ionViewDidLoad CadastrarUsuarioPage');
  }

}
