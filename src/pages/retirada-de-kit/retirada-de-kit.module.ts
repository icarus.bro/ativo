import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RetiradaDeKitPage } from './retirada-de-kit';

@NgModule({
  declarations: [
    RetiradaDeKitPage,
  ],
  imports: [
    IonicPageModule.forChild(RetiradaDeKitPage),
  ],
})
export class RetiradaDeKitPageModule {}
