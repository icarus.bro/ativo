import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { ReciboPage } from '../recibo/recibo';
import { Types, NotifyComponent } from '../../components/notify/notify';
import { NetworkService } from '../../services/network-service';
import { UserLoginProvider } from '../../providers/user-login/user-login';
import { KitWithdrawProvider } from '../../providers/kit-withdraw/kit-withdraw';

/**
 * Generated class for the RetiradaDeKitPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-retirada-de-kit',
  templateUrl: 'retirada-de-kit.html',
})
export class RetiradaDeKitPage {

  kitsToShow  = [];
  
    @ViewChild('notify')
    notify: NotifyComponent;
  
    constructor(public navCtrl: NavController, 
                public navParams: NavParams, 
                public kitWithdraw: KitWithdrawProvider, 
                private user: UserLoginProvider, 
                private menuCtrl: MenuController,
                public networkService: NetworkService) {}
  
    ionViewDidLoad() {
  
      this.notify.setNotify({
        type: Types.LOADING,
        message: 'Carregando Kits'
      });
  
      this.getKits({userId: this.user.user.idUser, offset: this.kitsToShow.length})
    }
  
    getKits(userDates: any){
      this.kitWithdraw.getKitsToWithdraw(userDates)
        .subscribe(data=>{
          this.kitsToShow = this.kitWithdraw.populateKitsToWithdraw(JSON.parse(data["_body"]));
  
          this.notify.clear();
  
          if(!this.kitsToShow.length){
            this.notify.setNotify({
              type: Types.NOTFOUND,
              message: 'Kits não disponíveis'
            });
          }
  
        }, error => {
  
        this.notify.clear();
        this.notify.setNotify({
          type: Types.WARNING,
          message: 'Não foi possível carregar'
        });
  
        })
    }
  
    goToReceipt(kit){
      console.log(kit);
      this.navCtrl.push(ReciboPage, {receipt: kit})
    }
  
  
}