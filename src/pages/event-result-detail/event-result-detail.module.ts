import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EventResultDetailPage } from './event-result-detail';

@NgModule({
  declarations: [
    EventResultDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(EventResultDetailPage),
  ],
})
export class EventResultDetailPageModule {}
