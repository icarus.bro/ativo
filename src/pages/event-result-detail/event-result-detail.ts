import { EventModalRegulamentPage } from './../event-modal-regulament/event-modal-regulament';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController } from 'ionic-angular';
import { EventModalRoutePage } from '../event-modal-route/event-modal-route';

@IonicPage()
@Component({
  selector: 'page-event-result-detail',
  templateUrl: 'event-result-detail.html',
})
export class EventResultDetailPage {

  pet: string = "results"
  modalRegulamentPage: any = EventModalRegulamentPage
  modalRoutePage: any = EventModalRoutePage

  ranking = [
    {  id: 34523, name: '1º Desafio do Teste', img: './assets/images/street-run.jpg', countEvents: 41 },
    {  id: 32997, name: 'Up Night Run 2017 - Fortaleza', img: './assets/images/swimming.jpg', countEvents: 28 },
    {  id: 34223, name: 'Etapa 5 Circuito Das Praias - Itanhaém Night Run', img: './assets/images/cross-training-string.jpg', countEvents: 24 },
    {  id: 34335, name: 'Gloss Run 2017', img: './assets/images/montain-run.jpg', countEvents: 14 },
    {  id: 34404, name: 'Color Fest® - Energy Edition', img: './assets/images/obstacles-2.jpg', countEvents: 64 },
    {  id: 34532, name: 'Corridas de Montanha Chapada Diamantina', img: './assets/images/bike.jpg', countEvents: 72 },
    {  id: 33980, name: '4ª Corrida e Caminhada Mais Saúde', img: './assets/images/others.jpg', countEvents: 100 }
  ]

  images;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public alertCtrl: AlertController,
              public modalCtrl: ModalController) {

    this.images = [
      {img: './assets/images/street-run.jpg'},
      {img: './assets/images/bike.jpg'},
      {img: './assets/images/street-run.jpg'},
      {img: './assets/images/montain-run.jpg'},
      {img: './assets/images/street-run.jpg'},
      {img: './assets/images/street-run.jpg'},
      {img: './assets/images/street-run.jpg'},
      {img: './assets/images/street-run.jpg'},
      {img: './assets/images/swimming.jpg'},
      {img: './assets/images/street-run.jpg'},
      {img: './assets/images/swimming.jpg'},
      {img: './assets/images/street-run.jpg'},
      {img: './assets/images/street-run.jpg'},
      {img: './assets/images/montain-run.jpg'},
      {img: './assets/images/street-run.jpg'},
      {img: './assets/images/street-run.jpg'},
    ]

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EventResultDetailPage');
  }



  showRegulament() {
    let modal = this.modalCtrl.create(this.modalRegulamentPage)
    modal.present();
  }

  showRoute() {
    let modal = this.modalCtrl.create(this.modalRoutePage)
    modal.present();
  }


}
