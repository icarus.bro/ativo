import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the UserPhotosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-user-photos',
  templateUrl: 'user-photos.html',
})
export class UserPhotosPage {

  images;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    
    this.images = [
      {img: './assets/images/street-run.jpg'},
      {img: './assets/images/bike.jpg'},
      {img: './assets/images/street-run.jpg'},
      {img: './assets/images/montain-run.jpg'},
      {img: './assets/images/street-run.jpg'},
      {img: './assets/images/street-run.jpg'},
      {img: './assets/images/street-run.jpg'},
      {img: './assets/images/street-run.jpg'},
      {img: './assets/images/swimming.jpg'},
      {img: './assets/images/street-run.jpg'},
      {img: './assets/images/swimming.jpg'},
      {img: './assets/images/street-run.jpg'},
      {img: './assets/images/street-run.jpg'},
      {img: './assets/images/montain-run.jpg'},
      {img: './assets/images/street-run.jpg'},
      {img: './assets/images/street-run.jpg'},
    ]

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserPhotosPage');
  }

}
