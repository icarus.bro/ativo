import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-advanced-filter',
  templateUrl: 'advanced-filter.html',
})
export class AdvancedFilterPage {



  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdvancedFilterPage');
  }

  dismissFilter() {
    this.viewCtrl.dismiss()
  }

}
