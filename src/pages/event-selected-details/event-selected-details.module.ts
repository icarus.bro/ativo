import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EventSelectedDetailsPage } from './event-selected-details';

@NgModule({
  declarations: [
    EventSelectedDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(EventSelectedDetailsPage),
  ],
})
export class EventSelectedDetailsPageModule {}
