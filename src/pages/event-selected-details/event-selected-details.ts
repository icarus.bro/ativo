import { EventModalRoutePage } from './../event-modal-route/event-modal-route';
import { EventModalRegulamentPage } from './../event-modal-regulament/event-modal-regulament';
import { EventModalKitPage } from './../event-modal-kit/event-modal-kit';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-event-selected-details',
  templateUrl: 'event-selected-details.html',
})
export class EventSelectedDetailsPage {

pet: string = "puppies"
//modal page
modalPage: any = EventModalRegulamentPage
modalPageRoute = EventModalRoutePage

 // Only presentation
 event: any
 name: string
 initials: string
 city: string
 local: string
 data: string
 hora: string
 modalidades: any[]
 retiradaKits: any[]

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public alertCtrl: AlertController,
              public modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
    // Only presentation
    this.event = this.navParams.get('event')
    this.name = this.event['name']
    this.initials = this.event['estado_sigla']
    this.city = this.event['cidade']
    this.local = this.event['local']
    this.data = this.event['data']
    this.hora = this.event['hora']
    this.modalidades = this.event['modalidades']

  }

  /**
   * Show withdrawal kits local
   */
  showAlert() {
    // Only presentation
    this.retiradaKits = this.event['retirada_kits']
    let local = this.retiradaKits[0]['local_retirada']
    let data = this.retiradaKits[0]['data_retirada']
    let hora = this.retiradaKits[0]['hora_retirada']
    let endereco = this.retiradaKits[0]['endereco_retirada']
    console.log( this.retiradaKits )

    let alert = this.alertCtrl.create({
      title: local,
      subTitle: 'A partir de: ' + data + ' - ' + hora,
      message: local + ', ' + ' ' + endereco,
      buttons: ['OK']
    });
    alert.present();
  }

  // showkits() {
  //   let modal = this.navCtrl.push(this.modalPage)
  // }

  showRegulament() {
    let modal = this.modalCtrl.create(this.modalPage)
    modal.present();
  }

  showRoute() {
    let modal = this.modalCtrl.create(this.modalPageRoute)
    modal.present();
  }

}
