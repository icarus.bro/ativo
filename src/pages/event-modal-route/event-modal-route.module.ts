import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EventModalRoutePage } from './event-modal-route';

@NgModule({
  declarations: [
    EventModalRoutePage,
  ],
  imports: [
    IonicPageModule.forChild(EventModalRoutePage),
  ],
})
export class EventModalRoutePageModule {}
