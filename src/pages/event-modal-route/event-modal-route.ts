import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-event-modal-route',
  templateUrl: 'event-modal-route.html',
})
export class EventModalRoutePage {

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EventModalRoutePage');
  }

  dismiss() {
    this.viewCtrl.dismiss()
  }

}
