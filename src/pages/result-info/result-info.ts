import { ViewChild } from '@angular/core';

import { EventsResultListPage } from './../events-result-list/events-result-list';
import { Component } from '@angular/core';

import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { TabsMainPage } from '../tabs-main/tabs-main';
import { Tabs } from 'ionic-angular/navigation/nav-interfaces';

@IonicPage()
@Component({
  selector: 'page-result-info',
  templateUrl: 'result-info.html',
})
export class ResultInfoPage {

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ResultInfoPage');
  }


}
