import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ResultInfoPage } from './result-info';

@NgModule({
  declarations: [
    ResultInfoPage,
  ],
  imports: [
    IonicPageModule.forChild(ResultInfoPage),
  ],
})
export class ResultInfoPageModule {}
