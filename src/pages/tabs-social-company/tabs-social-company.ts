import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FeedSpotifyPlaylistsPage } from '../feed-spotify-playlists/feed-spotify-playlists';
import { FeedPhrasesPage } from '../feed-phrases/feed-phrases';

@Component({
  selector: 'page-tabs-social-company',
  templateUrl: 'tabs-social-company.html',
})
export class TabsSocialCompanyPage {

  // tabFacebook: any = FeedFacebookPage;
  // tabInstagram: any = FeedInstagramPage;
  tabSpotify: any = FeedSpotifyPlaylistsPage;
  tabPhrases: any = FeedPhrasesPage;


  constructor(public navCtrl: NavController, public navParams: NavParams) {

  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad TabsSocialCompanyPage');
  }

}
