import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserResultsDetailsPage } from './user-results-details';

@NgModule({
  declarations: [
    UserResultsDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(UserResultsDetailsPage),
  ],
})
export class UserResultsDetailsPageModule {}
