import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { NoticiasModel } from '../../models/noticias-model';
import { SocialSharing } from '@ionic-native/social-sharing';

/**
 * Generated class for the NoticiaDetalhePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-noticia-detalhe',
  templateUrl: 'noticia-detalhe.html',
})
export class NoticiaDetalhePage {

  public noticia: NoticiasModel;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public menuCtrl: MenuController,
                private socialSharing: SocialSharing) {
          this.navParams.data.date = new Date(this.navParams.data.date.split(" ")[0]+"T"+this.navParams.data.date.split(" ")[1])
          this.noticia = this.navParams.data;
          //console.log(this.navParams.data);
    }

    regularShare(){
      this.socialSharing.share(this.navParams.data.title, null, this.navParams.data.imageurl, this.navParams.data.link);
    }

    doMenuToggle() {
      this.menuCtrl.toggle();
    }
  }
