import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams, MenuController } from 'ionic-angular';
import { KitWithdrawProvider } from '../../providers/kit-withdraw/kit-withdraw';
import JsBarcode from 'jsbarcode';
/**
 * Generated class for the ReciboPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-recibo',
  templateUrl: 'recibo.html',
})
export class ReciboPage {

  kit: any;
  receipt: any;

  btnEventShow = "remove";
  btnProductShow = "remove";

  @ViewChild('barcode') barcode: ElementRef;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              private kitWithdraw: KitWithdrawProvider,
              private menuCtrl: MenuController){

    this.receipt = {produtos: []};

    this.kit = this.navParams.get("receipt");

    this.kitWithdraw.getReceiptDetail(this.kit.idOrder)
      .subscribe(data=>{
        this.receipt = (JSON.parse(data["_body"]));
        console.log(this.receipt)
        JsBarcode(this.barcode.nativeElement, this.receipt.id_pedido, {
          displayValue: false,
        });
      }, error=>{
        
      }, ()=>{

      });

  }

  ionViewDidLoad() {
    JsBarcode(this.barcode.nativeElement, this.receipt.id_pedido);
  }

  changeEventIcon(event){
    if(this.btnEventShow == 'remove'){
      this.btnEventShow = 'add'
    }else{
      this.btnEventShow = 'remove'
    }
  }

  changeProductIcon(){
    if(this.btnProductShow == 'remove'){
      this.btnProductShow = 'add'
    }else{
      this.btnProductShow = 'remove'
    }
  }

  menuToggle(){
    this.menuCtrl.toggle();
  }

}
