import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController, Platform, ToastController } from 'ionic-angular';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { UserSignUpPage } from '../user-sign-up/user-sign-up';
import { UserForgotPasswordPage } from '../user-forgot-password/user-forgot-password';
import { TabsMainPage } from '../tabs-main/tabs-main';
import { UserLoginProvider } from '../../providers/user-login/user-login';
import { MenuProvider } from '../../providers/menu/menu';

import { Storage } from '@ionic/storage';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  public dataLoginForm: FormGroup;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams, 
    public loadingCtrl: LoadingController,
    public formBuilder: FormBuilder, 
    public alertCtrl: AlertController,
    //CODE - Banco do Brasil
		public fb: FormBuilder,
		private login: UserLoginProvider,
		public platform: Platform,
		public menuProvider: MenuProvider,
		private storage: Storage,
		private loadingController: LoadingController,
		private toastCtrl: ToastController,
  ) {

    this.dataLoginForm = this.formBuilder.group({
      usuario: ['',[Validators.required, Validators.minLength(3)]],
      senha: ['',[Validators.required, Validators.minLength(3)]]
    });

    //CODE - Banco do Brasil
    if(this.navParams.data.params == 'ourocard'){
			this.isOuro = true;
		}

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  presentLoading() {
    let loader = this.loadingCtrl.create({
      content: "Entrando",
      duration: 1000
    });
    loader.present();
  }

  // doLogin(){
  //   this.navCtrl.setRoot(TabsMainPage);
  // }

  signUp(): void{
    this.navCtrl.push(UserSignUpPage);
  }

  forgotPassword(): void{
    this.navCtrl.push(UserForgotPasswordPage);
  }

  //CODE - Banco do Brasil
  userMail: string
	userPassword: string
	items: any

	submitAttempt: boolean = false
	isOuro: boolean = false

	contas: any = [
			{ name: 'Ourocard', email: 'mathidios@gmail.com', password: 'tmb1987'},
			// ID 20278043
			{ name: 'Comum', email: 'jeanfarrell@gmail.com', password: 'jeanfarrell'},
			// ID 826570
			{ name: 'Nenhum', email: '', password: ''}
	];

	public loginForm = this.fb.group({
		email: ["",Validators.compose([Validators.pattern('^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$'), Validators.required])],
		password: ["",Validators.compose([Validators.minLength(6), Validators.required])]
	});

	consomeAPI(email: string, password: string){
		return new Promise ( (resolve, reject) => {
			this.login.login(email, password)
			.subscribe(data =>{
				try {
					this.items = JSON.parse(data['_body']);
					console.log( "CONSOME API" );
					console.log(this.items);

					resolve();
				}catch(e){
					reject(e);
				}
				this.submitAttempt = false;
			},err=> {
				reject(err);
			}, () =>{
			});
		})
	}

	doSelect() {
		let alert = this.alertCtrl.create();
		alert.setTitle('Contas');
		this.contas.forEach((val) => {
			alert.addInput({
				type: 'radio',
				label: val.name,
				value: val,
				checked: false
			})
		})

		alert.addButton('Cancel');
		alert.addButton({
			text: 'OK',
			handler: data => {
				this.loginForm = this.fb.group({
					email: [data.email,Validators.compose([Validators.pattern('^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$'), Validators.required])],
					password: [data.password,Validators.compose([Validators.minLength(6), Validators.required])]
				});
			}
		})
		alert.present();
	}

	doLogin(event) {

		this.submitAttempt = true;

		if(this.loginForm.valid){

			this.userMail = this.loginForm.controls['email'].value;
			this.userPassword = this.loginForm.controls['password'].value;

			let loading = this.loadingController.create({content : "Autenticando ...", cssClass: 'login-loading'});
			loading.present();

			this.consomeAPI(this.userMail, this.userPassword).then(()=>{
				this.login.getToken(this.userMail, this.userPassword)
					.subscribe(data => {

						var item = JSON.parse(data['_body']);
						this.login.user.token = item.token;

						console.log(data)
						loading.dismissAll()

						// MIX Panel
						// this.analytics.setData({
						// 		$name: this.items[0].ds_nome,
						// 		$email: this.userMail,
						// 		...this.items[0]
						// });
						console.log(this.items[0])
						this.preencheUsuario(this.items[0]);

						}, err =>{
							console.log('deu ruim no token');
						}, () => {

						});
			}).catch(err => {
				this.showAlertUsuarioIncorreto();
				this.submitAttempt = false;
				loading.dismissAll()

			});
		}
	}

	saveUser(user){

		this.storage.set('user', user);

	}

  // showAlertOuro() {
  //   let alert = this.alertCtrl.create({
  //     title: 'Ourocard',
  //     cssClass: 'ouro-card-alert',
  //     subTitle: 'Desculpe, mas você não tem acesso a este conteúdo',
  //     buttons: ['OK']
  //   });
  //   alert.present();
  // }

	preencheUsuario(usuario: any){

		this.login.populateUser(usuario, this.userMail)

		this.saveUser(this.login.user);

		if(this.login.user.email != ''){

			this.menuProvider.check(this.login.user.name);

			if(this.navParams.data.component){
				if(this.navParams.data.params){
					if(this.navParams.data.params == 'ourocard'){

	          let loading = this.loadingController.create({content : "Verificando Acesso", cssClass: 'login-loading'});
	          loading.present();

	        //   this.ourocardProvider.hasOurocard().then( isOuro => {

	        //     loading.dismissAll();

	        //     if(isOuro){

	        //       this.navCtrl.push(this.navParams.data.component, this.navParams.data.params)

	        //     } else {

	        //     	console.log('teste')

	        //     	this.navCtrl.pop()
	        //       this.showAlertOuro()
	        //     }
          //   }
          // ).catch( err => {
	        //     loading.dismissAll();
	        //   })

				}else{
					console.log("login")
	          		this.navCtrl.push(this.navParams.data.component, this.navParams.data.params)

				}

					// this.navCtrl.pop();
					// this.navCtrl.push(this.navParams.data.component, this.navParams.data.params);

				}else{
					console.log(this.navParams.data.component)
					this.navCtrl.pop();
					this.navCtrl.push(this.navParams.data.component);

				}
			}else{
				if(this.navParams.data == TabsMainPage){
					this.navCtrl.setRoot(TabsMainPage);
					return
				}


				this.navCtrl.pop({animate:false});
				this.navCtrl.push(this.navParams.data);
			}

		}else{
			console.log('erro aqui')
			this.menuProvider.loginButton = 'Entrar';
		}
	}

	launch(url) {
		this.platform.ready().then(() => {
			open(url, "_blank", "location=no");
		});
	}

	showAlertUsuarioIncorreto(){
		let prompt = this.alertCtrl.create({
			title: 'Login incorreto!',
			cssClass: 'ouro-card-alert',
			message: "Seu Email e/ou Senha estão incorretos, confira-os",

			buttons: [
			{
				text: 'OK',
				handler: data => {
					console.log('Faz nada!');
				}
			},
			]
		});

		prompt.present();
	}

	showAlert(){
		let prompt = this.alertCtrl.create({
			title: 'Esqueci minha senha',
			message: "Para redefinir sua senha, digite seu e-mail:",
			cssClass: 'alert-esqueci-senha',
			inputs: [
			{
				name: 'email',
				placeholder: 'Digite seu e-mail'
			},
			],

			buttons: [
			{
				text: 'Cancelar',
				handler: data => {
					console.log('cancelar clicado');
				}
			},
			{
				text: 'Enviar',
				handler: data => {
					console.log('enviar clicado');
					let mail: string = data.email
					console.log(mail.toLocaleLowerCase());
					this.login.resetPassword(mail.toLocaleLowerCase())
						.subscribe(data =>{
							console.log(JSON.parse(data["_body"]))
							let response = JSON.parse(data["_body"])
							let toast = this.toastCtrl.create({
								message: response.mensagem,
								duration: 3000,
								position: 'top'
							});

						toast.present();
						}, error => {
							let toast = this.toastCtrl.create({
								message: 'Erro ao enviar email',
								duration: 3000,
								position: 'top'
							});
						},() =>{

						})

				}
			}
			]
		});

		prompt.present();
	}

	popView(){
		this.navCtrl.pop();
	}

	toCadastro(){
		this.navCtrl.push(UserSignUpPage);
	}

}

  


