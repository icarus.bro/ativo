import { EventResultDetailPage } from './../event-result-detail/event-result-detail';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, LoadingController, AlertController, Platform } from 'ionic-angular';
import { MeuAtivoProvider } from '../../providers/meu-ativo/meu-ativo';
import { HomePageProvider } from '../../providers/home-page/home-page';
import { ResultsProvider } from '../../providers/results/results';
import { NetworkService } from '../../services/network-service';
import { Percurso } from '../../models/resultado-evento-model';
import { ResultadoAtletaDetalhesPage } from '../resultado-atleta-detalhes/resultado-atleta-detalhes';
import { NotifyComponent, Types } from '../../components/notify/notify';
import { MeusResultadoModel } from '../../models/meus-resultados-model';
//FOTOS
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file'
import { DomSanitizer } from '@angular/platform-browser';
import { FotoModel } from '../../models/foto-model';
import { ComprarFotoPage } from '../comprar-foto/comprar-foto';
import { ComprarFotoWebPage } from '../comprar-foto-web/comprar-foto-web';

declare var window: any;
@IonicPage()
@Component({
  selector: 'page-events-my-result-list',
  templateUrl: 'events-my-result-list.html',
})
export class EventsMyResultListPage {

  segmentedSelected: string = "results"

  @ViewChild('notifyEvent')
  notifyEvent: NotifyComponent;

  @ViewChild('notifyPhoto')
  notifyPhoto: NotifyComponent;

  private myPhotosToShow = new Array<FotoModel>()
  storageDirectory: string = '';

  public segmentOptionSelected :string =  'meusResultados';
  private myResultsToShow = new Array<MeusResultadoModel>()

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private meuAtivoProvider: MeuAtivoProvider,
    private homePageProvider: HomePageProvider,
    private resultsProvider: ResultsProvider,
    public networkService: NetworkService,
    public menuCtrl: MenuController,
    public loadingCtrl: LoadingController,
    //Fotos
    // public transfer: FileTransfer,
    // private file: File,
    private alertCtrl: AlertController,
    private platform: Platform,
    private sanitizer: DomSanitizer
  ) {

  }

  ionViewDidLoad() {
    this.buildMyResults();
    this.callPhotos();
  }

  openMyResults() {
    this.navCtrl.push(EventResultDetailPage);
  }

  buildMyResults(){

      this.notifyEvent.setNotify({
        type: Types.LOADING,
        message: 'Carregando seus resultados'
      });

      this.meuAtivoProvider.getMyResults()
        .subscribe(data => {

          this.populateWithMyResults(JSON.parse(data['_body']));

        }, error => {

          this.notifyEvent.setNotify({
            type: Types.WARNING,
            message: 'Não foi possível carregar'
          });

        });
    }

    populateWithMyResults(myResults: any){
      let results = []
      results = this.meuAtivoProvider.setMeusResultados(myResults);

      results.forEach(element =>{
        element.paceMedio  =  element.paceMedio.substring(3)
      })


      this.myResultsToShow = results;
      console.log( "LOG TO RESULTS" )
      console.log( this.myResultsToShow )

      if(!this.myResultsToShow.length){

        this.notifyEvent.setNotify({
          type: Types.NOTFOUND,
          message: 'Nenhum evento foi encontrado'
        });

      }else{
        this.notifyEvent.clear();

      }
    }

    goToResultDetail(result){

      let loading = this.loadingCtrl.create({
        content: 'Carregando...',
        cssClass: 'login-loading'
      });

      loading.present();

      //console.log(result)
      let eventResult: any;

      this.homePageProvider.getEventoPorId(result.idEvento)
        .subscribe(data=>{
          var detalhes = JSON.parse(data["_body"]);
          eventResult = this.homePageProvider.populateResultsEvents(JSON.parse(data["_body"]))[0]
          eventResult.dsRetiradaKits = detalhes[0].ds_retirada_kits;
          eventResult.regulamento = detalhes[0].ds_regulamento_txt;
          eventResult.percursos = this.addPercursos(detalhes[0].percursos);


          this.resultsProvider.getPositionsDetail(result.idEvento, result.numPeito)
            .subscribe(data=>{
            let myPosition = this.resultsProvider.populatePositionShowcase(JSON.parse(data["_body"]))[0];
            //console.log(myPosition)
            this.navCtrl.push(ResultadoAtletaDetalhesPage, {item: myPosition, evento: eventResult})
          },error=>{

          }, ()=>{
            loading.dismiss();
          })
        }, error =>{
          loading.dismiss();

        })

    }

    addPercursos(percursos: Array<any>){
      var percursosSerializados = new Array<Percurso>()

      for (let item of percursos){
        var serializado = new Percurso();
        serializado.modalidade = item.nm_modalidade;
        serializado.mapa = item.ds_mapa_google;
        percursosSerializados.push(serializado);
      }
      return percursosSerializados
    }


    menuToggle(){
      this.menuCtrl.toggle();
    }

    //
    //FOTOS
    showAlert() {
    let alert = this.alertCtrl.create({
      title: 'Download concluído!',
      subTitle: 'A fotos estão disponíveis na galeria do seu dispositivo',
      buttons: ['OK']
    });
    alert.present();
    }

    // downloadFiles(foto){

    // if(this.platform.is('cordova')){

    //   const promises = []

    //   const fileTransfer: FileTransferObject = this.transfer.create();

    //   foto.fotos.forEach((val) => {

    //     const path = `${this.file.dataDirectory}${val.id}.jpg`

    //     const filePromise = fileTransfer.download(val.url, path)
    //       .then( res => {

    //         window.cordova
    //         .plugins
    //         .imagesaver
    //         .saveImageToGallery(path, () => {}, err => console.log(`Save image error: ${err}`));

    //       })
    //       .catch( err => {
    //         console.log(`Download file error: ${err}`)
    //       })

    //       promises.push(filePromise)
    //   })

    //   Promise.all(promises).then(() => {
    //     this.showAlert();
    //   })
    // }
    // }

    callPhotos(){
      this.notifyPhoto.setNotify({
        type: Types.LOADING,
        message: 'Carregando suas fotos'
      });

      this.meuAtivoProvider.getMinhasFotos()
      .subscribe(data => {

        this.populateMyFotos(JSON.parse(data['_body']));

      }, error => {
        this.notifyPhoto.setNotify({
          type: Types.WARNING,
          message: 'Não foi possível carregar'
        });
      });
    }

    populateMyFotos(myPhotos: any){
      if(this.navParams.data.idEvento){
        let fotosEvento = this.meuAtivoProvider.setMinhasFotos(myPhotos);
        for (let fotos of fotosEvento){
          if(fotos.idEvento == this.navParams.data.idEvento){
            this.myPhotosToShow.push(fotos)
          }
        }
        console.log("vindo dos resultados")
      }else{
        this.myPhotosToShow = this.meuAtivoProvider.setMinhasFotos(myPhotos);
      }
      //console.log(this.myPhotosToShow)
      if(!this.myPhotosToShow.length){

        this.notifyPhoto.setNotify({
          type: Types.NOTFOUND,
          message: 'Nenhuma foto foi encontrada'
        });

      }else{
        this.notifyPhoto.clear();
      }
    }

    goToBuyPhotoWeb(imagens: FotoModel){
      imagens.url = this.transform("http://checkout.ativo.com/fotos?num_peito=" + imagens.numeroPeito +"&id_foto="+ imagens.fotos[0].id +"&id_evento="+ imagens.idEvento)
      this.navCtrl.push(ComprarFotoWebPage, imagens)
    }

    goToBuyPhoto(imagens: FotoModel){
      this.navCtrl.push(ComprarFotoPage, imagens)
    }

    transform(url) {
      return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    }

    getItems(ev: any) {
      // Reset items back to all of the items
      // this.initializeItems()

      // set val to the value of the searchbar
      let val = ev.target.value

      // if the value is an empty string don't filter the items
      if (val && val.trim() != '') {
        this.myResultsToShow = this.myResultsToShow.filter((result) => {
          return (result.nome.toLowerCase().indexOf(val.toLowerCase()) > -1)
        })
      } else {
        // response to empty string
      }
    }

}
