import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EventsMyResultListPage } from './events-my-result-list';

@NgModule({
  declarations: [
    EventsMyResultListPage,
  ],
  imports: [
    IonicPageModule.forChild(EventsMyResultListPage),
  ],
})
export class EventsMyResultListPageModule {}
