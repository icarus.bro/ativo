import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ResultResultPage } from './result-result';

@NgModule({
  declarations: [
    ResultResultPage,
  ],
  imports: [
    IonicPageModule.forChild(ResultResultPage),
  ],
})
export class ResultResultPageModule {}
