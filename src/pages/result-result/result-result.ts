import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-result-result',
  templateUrl: 'result-result.html',
})
export class ResultResultPage {

  results = [
    {  id: 34523, name: '1º Desafio do Teste', img: './assets/images/street-run.jpg', countEvents: 41 },
    {  id: 32997, name: 'Up Night Run 2017 - Fortaleza', img: './assets/images/swimming.jpg', countEvents: 28 },
    {  id: 34223, name: 'Etapa 5 Circuito Das Praias - Itanhaém Night Run', img: './assets/images/cross-training-string.jpg', countEvents: 24 },
    {  id: 34335, name: 'Gloss Run 2017', img: './assets/images/montain-run.jpg', countEvents: 14 },
    {  id: 34404, name: 'Color Fest® - Energy Edition', img: './assets/images/obstacles-2.jpg', countEvents: 64 },
    {  id: 34532, name: 'Corridas de Montanha Chapada Diamantina', img: './assets/images/bike.jpg', countEvents: 72 },
    {  id: 33980, name: '4ª Corrida e Caminhada Mais Saúde', img: './assets/images/others.jpg', countEvents: 100 }
  ]

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ResultResultPage');
  }

}
