import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserPurchaseDetailPage } from './user-purchase-detail';

@NgModule({
  declarations: [
    UserPurchaseDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(UserPurchaseDetailPage),
  ],
})
export class UserPurchaseDetailPageModule {}
