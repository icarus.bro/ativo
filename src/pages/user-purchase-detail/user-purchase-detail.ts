import { UserPurchaseModalKitPage } from './../user-purchase-modal-kit/user-purchase-modal-kit';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ModalController } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-user-purchase-detail',
  templateUrl: 'user-purchase-detail.html',
})
export class UserPurchaseDetailPage {

  purchasesData: any
  purchases: any[]

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public viewCtrl: ViewController,
              public modalCtrl: ModalController) {

              this.purchasesData = this.navParams.get('purchase')

  }

  ionViewDidLoad() {

  }

  presentKitsDatail() {
    let contactModal = this.modalCtrl.create(UserPurchaseModalKitPage)
    contactModal.present();
  }

}
