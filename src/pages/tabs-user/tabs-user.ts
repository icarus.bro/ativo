import { EventsUserListPage } from './../events-user-list/events-user-list';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { UserProfilePage } from '../user-profile/user-profile';
// import { SuperTabsController } from 'ionic2-super-tabs';
import { UserPhotosPage } from '../user-photos/user-photos';
import { EventsResultListPage } from './../events-result-list/events-result-list';

@Component({
  selector: 'page-tabs-user',
  templateUrl: 'tabs-user.html',
})
export class TabsUserPage {

  tabProfile: any = UserProfilePage
  tabMyPhotos: any = UserPhotosPage
  tabMyResults: any = EventsUserListPage

  showTitles: boolean = true;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TabsUserPage');
  }

}
