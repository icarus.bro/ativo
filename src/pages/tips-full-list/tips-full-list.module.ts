import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TipsFullListPage } from './tips-full-list';

@NgModule({
  declarations: [
    TipsFullListPage,
  ],
  imports: [
    IonicPageModule.forChild(TipsFullListPage),
  ],
})
export class TipsFullListPageModule {}
