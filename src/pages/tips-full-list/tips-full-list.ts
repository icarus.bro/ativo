import { InfoTipsReadPage } from './../info-tips-read/info-tips-read';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-tips-full-list',
  templateUrl: 'tips-full-list.html',
})
export class TipsFullListPage {


  page: any = InfoTipsReadPage
  articles = [
    {id: '0', img: 'https://do1pouckcwxot.cloudfront.net/brasil/uploads/2017/11/09163944/feridas.jpg', title: 'Estudo aponta que feridas diurnas são curadas 60% mais rápido', author: 'PEDRO CUNACIA', date: '9 DE NOVEMBRO DE 2017', text: 'É comum durante o dia, principalmente para quem pratica esportes, o aparecimento de algum machucado no joelho, ou um ralado no cotovelo ou qualquer outro lugar do corpo. Entretanto, você sabia que feridas que acontecem durante o dia podem ter cicatrização 60% mais rápida do que machucados noturnos? Estranho? Pois é, foi a constatação de pesquisadores do Reino Unido. Segundo um estudo liderado por cientistas do Laboratório de Biologia Molecular do Conselho de Pesquisa Médica (MRC) em Cambridge, no Reino Unido, as células da pele realizam um processo de reparo dos cortes e queimaduras de uma forma mais eficaz durante o dia. Na experiência feita pelos pesquisadores e publicada na Science Translation Medicine, o objetivo foi mostrar como os mecanismos do corpo regulam a cicatrização das feridas nas duas partes do dia.'},
    {id: '2', img: 'https://do1pouckcwxot.cloudfront.net/brasil/uploads/2017/02/08104137/memoria.jpg', title: 'Estudo confirma que correr faz bem para o cérebro e cria novos neurônios', author: 'CAMILABROGLIATO', date: '27 DE OUTUBRO DE 2017', text: 'O início dos efeitos do exercício sobre o cérebro é rápido e evolui ao longo do tempo. Inicialmente, os níveis de neurotransmissores e o fluxo sanguíneo são alterados, seguidos de uma regulação positiva de fatores de crescimento e nascimento de novos neurônios no hipocampo. Em testes com ratos, a corrida resultou em mudanças estruturais e funcionais em regiões cerebrais importantes para a cognição, como o hipocampo e o córtex. Nos seres humanos, a atividade física melhorou a memória dependente do hipocampo e a função do córtex pré-frontal –  além de ajudar a manter neurônios vivos por mais tempo.'},
    {id: '3', img: 'https://do1pouckcwxot.cloudfront.net/brasil/uploads/2017/09/01173948/Untitled-design-15.jpg', title: 'O bicarbonato de sódio melhora a performance na corrida?', author: 'PEDRO LOPES', date: '6 DE SETEMBRO DE 2017', text: 'O bicarbonato de sódio é visto como um produto curinga para a limpeza de utensílios domésticos. O caráter versátil da substância, segundo pesquisadores, pode se estender ao universo esportivo, mais especificamente à capacidade de auxiliar na recuperação de atletas.'},
    {id: '4', img: 'https://do1pouckcwxot.cloudfront.net/brasil/uploads/2017/08/18175246/fisio.jpg', title: 'Copo meio vazio: álcool x desempenho na corrida', author: 'ANDRÉ SENDER', date: '18 DE AGOSTO DE 2017', text: 'Algumas latinhas de cerveja depois do trabalho, uns drinques durante o jantar na sexta-feira, mesmo com o longão marcado para sábado… Essas podem parecer atitudes comuns. Até porque muitas vezes associamos a ingestão de bebida alcoólica ao prazer e ao relaxamento. Mas o consumo de álcool pode atrapalhar seu rendimento em treinos e provas e até anular o esforço que você faz para evoluir na corrida.'}
  ]




  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TipsFullListPage')

  }

  openTip() {
    let modal = this.modalCtrl.create(this.page)
    modal.present()
  }


}
