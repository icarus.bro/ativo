import { TipsFullListPage } from './../tips-full-list/tips-full-list';
import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-info-tips',
  templateUrl: 'info-tips.html',
})
export class InfoTipsPage {
  page: any
  tips = [
    { id: '11', name: 'Saúde', img: './assets/images/tip_1.svg', countTips: 8 },
    { id: '12', name: 'Nutrição', img: './assets/images/tip_2.svg', countTips: 2 },
    { id: '13', name: 'Mulher', img: './assets/images/tip_3.svg', countTips: 9 },
    { id: '14', name: 'Galerias', img: './assets/images/tip_4.svg', countTips: 1 },
    { id: '15', name: 'Vídeos', img: './assets/images/tip_5.svg', countTips: 4 },
    { id: '16', name: 'Expert', img: './assets/images/tip_6.svg', countTips: 3 },
    { id: '17', name: 'Repórter Ativo', img: './assets/images/tip_7.svg', countTips: 9 }
  ]

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public modalCtrl: ModalController) {
    this.page = TipsFullListPage
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InfoTipsPage');
  }

  listTipsCategory() {
    this.navCtrl.push(this.page)
  }

}
