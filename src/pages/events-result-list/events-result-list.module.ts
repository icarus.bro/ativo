import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EventsResultListPage } from './events-result-list';

@NgModule({
  declarations: [
    EventsResultListPage,
  ],
  imports: [
    IonicPageModule.forChild(EventsResultListPage),
  ],
})
export class EventsResultListPageModule {}
