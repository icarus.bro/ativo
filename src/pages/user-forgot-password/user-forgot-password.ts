import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

/**
 * Generated class for the UserForgotPasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-user-forgot-password',
  templateUrl: 'user-forgot-password.html',
})
export class UserForgotPasswordPage {


  forgotPasswordForm: FormGroup;

  constructor(public formBuilder: FormBuilder,
    public navCtrl: NavController, 
    public navParams: NavParams) {
    this.forgotPasswordForm = this.formBuilder.group({
      mail: ['',[Validators.required, Validators.minLength(3)]]
      //acrescentar os demais
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EsqueciSenhaPage');
  } 

  onSubmit(): void{
    console.log(this.forgotPasswordForm.value);
    
  }

}
