import { InfoNewsPage } from './../info-news/info-news';
import { Component } from '@angular/core';
import { EventsFullCalendarPage } from '../events-full-calendar/events-full-calendar';
import { EventsTrendsCalendarPage } from '../events-trends-calendar/events-trends-calendar';
import { EventsResultListPage } from './../events-result-list/events-result-list';
import { TabsNewsTipsPage } from '../tabs-news-tips/tabs-news-tips';
import { UserLoginProvider } from '../../providers/user-login/user-login';
import { NavController, NavParams, Platform } from 'ionic-angular';
import { Device } from '@ionic-native/device';
import { LoginPage } from '../login/login';
import { ResultsPage } from '../results/results';
import { DicasPage } from '../dicas/dicas';

@Component({
  selector: 'page-tabs-main',
  templateUrl: 'tabs-main.html',
})
export class TabsMainPage {

  tabEventsFullCalendar: any
  tabEventsTrendsCalendar: any
  tabNewsTip: any
  tabEventsResult: any

  //CODE - Banco do Brasil
  resultsPage: any;
  loginPage: any;
	// class inicialized to checkin
	checkInClass: boolean
	checkInFriendsClass: boolean
	// currentPageToCheckin: any = this.getCurrentUserFacebook()

  constructor(public usuarioProvider: UserLoginProvider,
              public navController: NavController,
              private navParams: NavParams,
    ){
      this.tabEventsFullCalendar = EventsFullCalendarPage
      this.tabEventsTrendsCalendar = EventsFullCalendarPage
      this.tabNewsTip = DicasPage
      this.tabEventsResult = ResultsPage
  }

  //CODE - Banco do Brasil

  ionViewWillEnter() {
		this.checkInClass = true
		this.checkInFriendsClass = false

    // this.ourocardProvider.hasOurocard().then( isOuro => {
    //   if(isOuro){
    //     this.showCadeadoOurocard = true
    //   }else{
    //     this.showCadeadoOurocard = false
    //   }
    // })
  }

  openPage(page) {
    if(this.usuarioProvider.user.name != '' || page.component == TabsMainPage){
      if(page.component != null){
        // if((page.component === DicasPage) && (page.params === 'ourocard')){
        //   let loading = this.loadingCtrl.create({content : "Verificando Acesso", cssClass: 'login-loading'});
        //   loading.present();
        //   this.ourocardProvider.hasOurocard().then( isOuro => {
        //     loading.dismissAll();
        //     if(isOuro){
        //       this.navController.push(page.component, page.params)
        //     } else {
        //       this.showAlertOuro()
        //     }
        //   }).catch( err => {
        //     loading.dismissAll();
        //   })
        // }
        // else {
        //   this.navController.push(page.component);
        // }
      }else{
        this.navController.setRoot(TabsMainPage);
      }
    }else{
      this.navController.push(LoginPage, Object.assign({}, page));
    }
  }
}
