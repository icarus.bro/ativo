import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ResultAthleteDetailsPage } from './result-athlete-details';

@NgModule({
  declarations: [
    ResultAthleteDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(ResultAthleteDetailsPage),
  ],
})
export class ResultAthleteDetailsPageModule {}
