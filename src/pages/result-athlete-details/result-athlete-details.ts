import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-result-athlete-details',
  templateUrl: 'result-athlete-details.html',
})
export class ResultAthleteDetailsPage {

  //recebe parametros
  athlete: any[]
  athleteName: string
  athleteRace: string

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public viewCtrl: ViewController) {

              this.athlete = navParams.get('ranking')
              // Appear in modals header
              this.athleteName = this.athlete['nome']
              this.athleteRace = this.athlete['id_evento']

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ResultAthleteDetailsPage');
    this.loadDataAthletes()

  }

  /**
   * Close details modal
   * @param
   *
   */
  dismiss() {
    this.viewCtrl.dismiss()
  }

  /**
   * Load athletes data selected
   * @param idAtlhetes
   *
   */
  loadDataAthletes() {
    return this.athlete
  }

}
