import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';

/**
 * Generated class for the ComprarFotoWebPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-comprar-foto-web',
  templateUrl: 'comprar-foto-web.html',
})
export class ComprarFotoWebPage {
  url: string = "";
  urlToIframe: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private platform: Platform, private sanitizer: DomSanitizer) {
    
  }

  transform(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }


  ionViewDidLoad() {
    this.url = ("http://checkout.ativo.com/fotos?num_peito=" + this.navParams.data.numeroPeito +"&id_foto="+ this.navParams.data.fotos[0].id +"&id_evento="+ this.navParams.data.idEvento);

    this.urlToIframe = this.transform(this.url);
  }

}
