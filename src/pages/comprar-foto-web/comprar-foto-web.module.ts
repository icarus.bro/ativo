import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComprarFotoWebPage } from './comprar-foto-web';

@NgModule({
  declarations: [
    ComprarFotoWebPage,
  ],
  imports: [
    IonicPageModule.forChild(ComprarFotoWebPage),
  ],
})
export class ComprarFotoWebPageModule {}
