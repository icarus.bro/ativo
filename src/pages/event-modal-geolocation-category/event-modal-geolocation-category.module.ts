import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EventModalGeolocationCategoryPage } from './event-modal-geolocation-category';

@NgModule({
  declarations: [
    EventModalGeolocationCategoryPage,
  ],
  imports: [
    IonicPageModule.forChild(EventModalGeolocationCategoryPage),
  ],
})
export class EventModalGeolocationCategoryPageModule {}
