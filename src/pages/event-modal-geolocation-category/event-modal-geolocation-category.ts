import { Cidade } from './../../models/cidade-model';
import { CalendarProvider } from './../../providers/calendar/calendar';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { Localization } from '../../util/localization';


@IonicPage()
@Component({
  selector: 'page-event-modal-geolocation-category',
  templateUrl: 'event-modal-geolocation-category.html',
})
export class EventModalGeolocationCategoryPage {

  currentLocation: string
  locates: any[]
  currentGeolocation: string

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public viewCtrl: ViewController,
              private localization: Localization,
              private calendarProvider: CalendarProvider) {

  }


  ionViewDidLoad() {
    this.currentGeolocation = this.navParams.get('geolocation')
    this.getAllLocatesToFilter()
  }

  /**
   * List all locates to
   */
  getAllLocatesToFilter() {
    this.localization.getAllLocates()
      .then((response: any) => {
        this.locates = response
        console.log(this.locates)
      }).catch((error) => {
        console.log( 'Não foi possível carregar cidades.' )
      })
  }

  /**
   * Exec search in list
   * @param ev
   */
  getItems(ev: any) {
    // set val to the value of the searchbar
    let val = ev.target.value

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.locates = this.locates.filter((locate) => {
        return (
          locate.ds_cidade.toLowerCase().indexOf(val.toLowerCase()) > -1
        )
      })
    }  else {
      // response to empty string
      this.getAllLocatesToFilter()
    }
  }

  closeFilter() {
    this.viewCtrl.dismiss()
  }

  executeSearchList(ev: any) {

    console.log( "ENV" )
    console.log( ev )

    this.currentGeolocation = ev.target.value

    console.log(this.currentGeolocation)

    this.closeFilter()
  }

}
