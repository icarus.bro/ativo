import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EventsUserListPage } from './events-user-list';

@NgModule({
  declarations: [
    EventsUserListPage,
  ],
  imports: [
    IonicPageModule.forChild(EventsUserListPage),
  ],
})
export class EventsUserListPageModule {}
