import { UserMyResultsPage } from './../user-my-results/user-my-results';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-events-user-list',
  templateUrl: 'events-user-list.html',
})
export class EventsUserListPage {
  events
  page = UserMyResultsPage

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    this.initializeItems()
  }

   /**
   * Ionitialize values hardcoded
   */
  initializeItems() {
    // hardcoded data to calendar, list of categories
    this.events = [
      {
        id: '34523',
        name: '1º Desafio do Teste',
        local: 'Praia Central de Bombinhas',
        img: './assets/images/street-run.jpg',
        data: '2017-10-20',
        hora: '09:00',
        pais: 'Brasil',
        estado_sigla: 'SC',
        estado_nome: 'Santa Catarina',
        cidade: 'Bombinhas',
        modalidades: [{'id_modalidade': '44349', 'nm_modalidade': '4k'}],
        data_fim_inscricao:'2017-10-19',
        retirada_kits: [{
          'data_retirada': '2017-10-20',
          'hora_retirada': '10:00',
          'local_retirada': 'Norte Marketing',
          'endereco_retirada': 'Rua Estela Barros, 336',
          'bairro_retirada': 'Limão',
          'cidade_retirada': 'Sâo Paulo'
        }]
      },
      {
        id: '32997',
        name: 'Up Night Run 2017',
        local: 'Centro de Eventos do Ceará',
        img: './assets/images/swimming.jpg',
        data: '2017-10-21',
        hora: '20:00',
        pais: 'Brasil',
        estado_sigla: 'CE',
        estado_nome: 'Fortaleza',
        cidade: 'Fortaleza',
        modalidades: [
          {'id_modalidade': '34830', 'nm_modalidade': '5k'},
          {'id_modalidade': '34831', 'nm_modalidade': '10k'}
        ],
        data_fim_inscricao:'2017-10-16',
        retirada_kits: [{
          'data_retirada': '2017-10-20',
          'hora_retirada': '10:00',
          'local_retirada': 'Norte Marketing',
          'endereco_retirada': 'Rua Estela Barros, 336',
          'bairro_retirada': 'Limão',
          'cidade_retirada': 'Sâo Paulo'
        }]
      },
      {
        id: '34223',
        name: 'Circuito Das Praias',
        local: 'Praia do Cibratel 2',
        img: './assets/images/cross-training-string.jpg',
        data: '2017-10-20',
        hora: '18:00',
        pais: 'Brasil',
        estado_sigla: 'SP',
        estado_nome: 'São Paulo',
        cidade: 'Itanhaém',
        modalidades: [
          { 'id_modalidade': '43115', 'nm_modalidade': '5k' },
          { 'id_modalidade': '43116', 'nm_modalidade': '10k' },
          { 'id_modalidade': '43117', 'nm_modalidade': '5k' }

        ],
        data_fim_inscricao:'2017-10-16',
        retirada_kits: [{
          'data_retirada': '2017-10-20',
          'hora_retirada': '10:00',
          'local_retirada': 'Norte Marketing',
          'endereco_retirada': 'Rua Estela Barros, 336',
          'bairro_retirada': 'Limão',
          'cidade_retirada': 'Sâo Paulo'
        }]

      }
    ]
  }

  /**
   * Exec search in list
   * @param ev
   */
  getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems()

    // set val to the value of the searchbar
    let val = ev.target.value

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.events = this.events.filter((event) => {
        return (event.name.toLowerCase().indexOf(val.toLowerCase()) > -1)
      })
    } else {
      // response to empty string
    }
  }

}
