import { ResultTrendPage } from './../result-trend/result-trend';
import { ResultResultPage } from './../result-result/result-result';
import { ResultPhotoPage } from './../result-photo/result-photo';
import { ResultInfoPage } from '../result-info/result-info';
import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-tabs-results',
  templateUrl: 'tabs-results.html'
})
export class TabsResultsPage {

  resultInfoRoot: any = ResultInfoPage
  resultPhotoRoot: any = ResultPhotoPage
  resultResultRoot: any = ResultResultPage
  resultTrendRoot: any = ResultTrendPage


  constructor(public navCtrl: NavController) {}

}
