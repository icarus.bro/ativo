import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TabsResultsPage } from './tabs-results';

@NgModule({
  declarations: [
    TabsResultsPage,
  ],
  imports: [
    IonicPageModule.forChild(TabsResultsPage),
  ]
})
export class TabsResultsPageModule {}
