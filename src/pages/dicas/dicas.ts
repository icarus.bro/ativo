import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, MenuController } from 'ionic-angular';

import 'rxjs/add/operator/toPromise';
import { Types, NotifyComponent } from '../../components/notify/notify';
import { Http } from '@angular/http';
import { NetworkService } from '../../services/network-service';
import { NoticiasProvider } from '../../providers/noticias/noticias';
import { NoticiaDetalhePage } from '../noticia-detalhe/noticia-detalhe';

/**
 * Generated class for the DicasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-dicas',
  templateUrl: 'dicas.html',
})
export class DicasPage {

	@ViewChild('notify')
	notify: NotifyComponent;

	newsEvents: Array<any>;

	noticiaDetalhe = NoticiaDetalhePage;
	category = 36703;
	qtd = 5
	title = 'Dicas de Treino';
	urls = {
		// 'dicas-ourocard': 'http://www.ativo.com/apprss?type=post&category=ID&qtd=4',
		// 'dicas': 'http://www.atletas.info/apprss?type=post&category=ID&qtd=4'
		// 'dicas-ourocard': 'http://www.ativo.com/apprss?type=post&category=ID&qtd=4',
		// 'dicas': 'http://www.ativo.com/apprss?type=post&category=ID&qtd=4'
		'dicas-ourocard' : 'http://www.atletas.info/apprss/?type=post&category=ID&qtd=10',
		'dicas' : 'http://www.atletas.info/apprss/?type=post&category=ID&qtd=10'
	}

	constructor(private noticiasProvider: NoticiasProvider,
		public networkService: NetworkService,
		public menuCtrl: MenuController,
		private navParans: NavParams,
		private http: Http) {

	}

	ionViewDidLoad() {
		this.checkToLoad();
	}

	checkToLoad(){

		this.notify.setNotify({
			type: Types.LOADING,
			message: 'Carregando conteúdo'
		  });

		// this.loadDicas(this.urls['dicas']);
		this.loadDicas();
		// if(this.navParans.data == 'ourocard'){
		// 	this.loadDicas(this.urls['dicas-ourocard']);
		// 	this.title = 'Dicas de Treino Ourocard';
		// }
		// else{
		// 	this.loadDicas(this.urls['dicas']);
		// }
	}

	loadDicas() {
		this.http.get('http://www.atletas.info/apprss/?type=post&category=ID&qtd=40')
		// this.http.get('http://www.ativo.com/apprss?type=post&category=ID&qtd=40')
		.toPromise()
		.then( data => {
			this.newsEvents = JSON.parse(data["_body"]);
			this.notify.clear();
			console.log( this.newsEvents );

		})
		.catch( err => {
			this.notify.setNotify({
				type: Types.WARNING,
				message: err + 'Não foi possível carregar'
			});
		})
	}

	doMenuToggle() {
		this.menuCtrl.toggle();
	}
}
