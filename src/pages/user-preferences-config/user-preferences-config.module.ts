import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserPreferencesConfigPage } from './user-preferences-config';

@NgModule({
  declarations: [
    UserPreferencesConfigPage,
  ],
  imports: [
    IonicPageModule.forChild(UserPreferencesConfigPage),
  ],
})
export class UserPreferencesConfigPageModule {}
