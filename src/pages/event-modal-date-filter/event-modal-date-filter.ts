import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-event-modal-date-filter',
  templateUrl: 'event-modal-date-filter.html',
})
export class EventModalDateFilterPage {

  startDate
  endDate

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EventModalDateFilterPage');
  }


  /**
   * Close dates modal
   *
   */
  dismissDateFilter() {
    this.viewCtrl.dismiss()
  }

  /**
   * Set dates to filter
   * @param startDate
   * @param endDate
   *
   */
  setDatesToFilter() {

  }

}
