import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EventModalDateFilterPage } from './event-modal-date-filter';

@NgModule({
  declarations: [
    EventModalDateFilterPage,
  ],
  imports: [
    IonicPageModule.forChild(EventModalDateFilterPage),
  ],
})
export class EventModalDateFilterPageModule {}
