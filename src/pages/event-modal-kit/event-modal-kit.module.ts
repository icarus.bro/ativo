import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EventModalKitPage } from './event-modal-kit';

@NgModule({
  declarations: [
    EventModalKitPage,
  ],
  imports: [
    IonicPageModule.forChild(EventModalKitPage),
  ],
})
export class EventModalKitPageModule {}
