import { ResultAthleteDetailsPage } from './../result-athlete-details/result-athlete-details';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-event-ranking',
  templateUrl: 'event-ranking.html',
})
export class EventRankingPage {

  // modalResultAthleteDetails = ResultAthleteDetailsPage
  segmentOptionSelected: string = 'ranking'

  // Data hardcoded
  ranking = [
    {
      id_usuario: "20310543",
      id_evento: "24227",
      id_resultado: "54147810",
      nome: "Ronaldo Tomaz Corrêa",
      sexo: "m",
      num_peito: "4362",
      modalidade: "10k",
      percurso: "10",
      categoria: "ELITM10",
      equipe: "",
      tempo_total: "00:44:42",
      tempo_bruto: "00:44:42",
      pace: "00:04:28",
      itens: {
          classificacao_sexo: "1",
          classificacao_categoria: "1",
          classificacao_total: "1",
          equipe: "",
          velocidade_media: "13,4"
      },
      classificacao_total: "1"
    }
  ]

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public modalCtrl: ModalController) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EventRankingPage');
    this.getAllPositionRace()
  }

  /**
   * List the overall result of the participated race
   * @param idEvent
   *
   */
    getAllPositionRace() {
      return this.ranking
    }


}
