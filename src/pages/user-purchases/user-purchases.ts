import { UserPurchaseDetailPage } from './../user-purchase-detail/user-purchase-detail';
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { NotifyComponent, Types } from '../../components/notify/notify';
import { MeusPedidosProvider } from '../../providers/meus-pedidos/meus-pedidos';
import { ReciboPage } from '../recibo/recibo';

@Component({
  selector: 'page-user-purchases',
  templateUrl: 'user-purchases.html',
})
export class UserPurchasesPage {


  purchases = [
    {
      id: '11',
      purchaseName: 'Bravus Race 2017',
      cosumerName: 'Durval Nesfit',
      payment: 'AMEX',
      status: 'pago',
      price: 199.99,
      taxService: 14.00,
      raceCategory: 'Corrida de Obstáculos',
      inscriptionLevel: 'Peloptão de Elite',
      kit: true
    }
  ]

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public modalCtrl: ModalController,
              //CODE - Banco do Brasil
              public meusPedidosProvider: MeusPedidosProvider) {

      this.myOrdersToShow = []; 
  }

  // ionViewDidLoad() {
  //   console.log('ionViewDidLoad UserPurchasesPage');
  // }

  presentModalPurchase() {
    let modal = this.modalCtrl.create(UserPurchaseDetailPage);
    modal.present();
  }

  //CODE - Banco do Brasil

  @ViewChild('notify')
  notify: NotifyComponent

  myOrdersToShow: Array<any>;

  ionViewDidLoad(){
    this.getMeusPedidos();
  }

  getMeusPedidos(){
  
      this.meusPedidosProvider.getMeusPedidos()
        .subscribe(data =>{
  
          let orders = [] 
          orders = JSON.parse(data['_body']);
  
          orders.forEach(element=>{
            element.data_pedido = new Date(element.data_pedido.split(" ")[0]+"T03:00:00")
            //console.log(element)
          })
  
          this.myOrdersToShow = orders;
          //console.log(this.myOrdersToShow)
          
          if(!this.myOrdersToShow.length){
            this.notify.setNotify({
              type: Types.NOTFOUND,
              message: "Nenhum pedido foi encontrado"
            });
          }
        });
  
    };
  
    goToReceipt(pedido: any){
      if(pedido.pedido_status == "Cancelado"){
        this.navCtrl.push(UserPurchaseDetailPage, {order: pedido});
      }else{
        this.navCtrl.push(ReciboPage, {receipt: {idOrder: pedido.id_pedido}})
      }
    }
  
    goToDetalhes(pedido: any){
      this.navCtrl.push(UserPurchaseDetailPage, {order: pedido});
    }
  
    // menuToggle(){
    //   this.menuCtrl.toggle();
    // }
  
    // home(){
    //   this.navCtrl.setRoot(HomePage);
    // }
    




}
