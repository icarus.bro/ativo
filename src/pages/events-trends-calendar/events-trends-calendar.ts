import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-events-trends-calendar',
  templateUrl: 'events-trends-calendar.html',
})
export class EventsTrendsCalendarPage {
// Data hardcoded
  trends = [
    { enrolled: '34', img: './assets/images/event_one.jpg' },
    { enrolled: '57', img: './assets/images/event_two.jpg' },
    { enrolled: '67', img: './assets/images/event_three.jpg' },
    { enrolled: '78', img: './assets/images/event_four.jpg' },
    { enrolled: '43', img: './assets/images/event_five.jpg' },
    { enrolled: '87', img: './assets/images/event_six.jpg' }
  ]

  constructor(public navCtrl: NavController,
              public navParams: NavParams) {

  }

}
