import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ResultTrendPage } from './result-trend';

@NgModule({
  declarations: [
    ResultTrendPage,
  ],
  imports: [
    IonicPageModule.forChild(ResultTrendPage),
  ],
})
export class ResultTrendPageModule {}
