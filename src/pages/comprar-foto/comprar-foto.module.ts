import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComprarFotoPage } from './comprar-foto';

@NgModule({
  declarations: [
    ComprarFotoPage,
  ],
  imports: [
    IonicPageModule.forChild(ComprarFotoPage),
  ],
})
export class ComprarFotoPageModule {}
