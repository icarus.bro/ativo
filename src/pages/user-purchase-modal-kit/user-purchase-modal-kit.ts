import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-user-purchase-modal-kit',
  templateUrl: 'user-purchase-modal-kit.html',
})
export class UserPurchaseModalKitPage {

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserPurchaseModalKitPage');
  }

  dismissDetailKit() {
    this.viewCtrl.dismiss()
  }


}
