import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserPurchaseModalKitPage } from './user-purchase-modal-kit';

@NgModule({
  declarations: [
    UserPurchaseModalKitPage,
  ],
  imports: [
    IonicPageModule.forChild(UserPurchaseModalKitPage),
  ],
})
export class UserPurchaseModalKitPageModule {}
