import { EventRankingPage } from './../event-ranking/event-ranking';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, LoadingController, AlertController, Platform } from 'ionic-angular';
import { Percurso } from '../../models/resultado-evento-model';
import { Types, NotifyComponent } from '../../components/notify/notify';
import { MeusResultadoModel } from '../../models/meus-resultados-model';
import { MeuAtivoProvider } from '../../providers/meu-ativo/meu-ativo';
import { HomePageProvider } from '../../providers/home-page/home-page';
import { NetworkService } from '../../services/network-service';
import { ResultsProvider } from '../../providers/results/results';
import { ResultadoAtletaDetalhesPage } from '../resultado-atleta-detalhes/resultado-atleta-detalhes';

@IonicPage()
@Component({
  selector: 'page-results',
  templateUrl: 'results.html',
})
export class ResultsPage {

  // hardcoded data to rasult, list of past events
  pastEvents = [
    {
      id_evento: "34747",
      ds_evento: "Black Trunk Race",
      ds_estado_name: "São Paulo",
      dt_evento: "2018-04-15 00:00:00",
      img_banner_evento: "https://checkout.akamaized.net/upload/evento/34747/img_banner_evento.jpg",
    },
    {
      id_evento: "34031",
      ds_evento: "Stadium – 3º Desafio do Batalhão Anchieta",
      ds_estado_name: "São Paulo",
      dt_evento: "2017-12-17 00:00:00",
      img_banner_evento: "https://checkout.akamaized.net/upload/evento/34031/img_banner_evento.jpg",
    },
  ]
  page = EventRankingPage

  @ViewChild('notifyEvent')
  notifyEvent: NotifyComponent;

  public segmentOptionSelected :string =  'meusResultados';
  private myResultsToShow = new Array<MeusResultadoModel>()

constructor(public navCtrl: NavController,
            public navParams: NavParams,
            private meuAtivoProvider: MeuAtivoProvider,
            private homePageProvider: HomePageProvider,
            private resultsProvider: ResultsProvider,
            public networkService: NetworkService,
            public menuCtrl: MenuController,
            public loadingCtrl: LoadingController
          ) {}

ionViewDidLoad() {
  this.buildMyResults()
  this.pastEvents
}

buildMyResults(){

  this.notifyEvent.setNotify({
    type: Types.LOADING,
    message: 'Carregando seus resultados'
  });

  this.meuAtivoProvider.getMyResults()
    .subscribe(data => {

      this.populateWithMyResults(JSON.parse(data['_body']));

    }, error => {

      this.notifyEvent.setNotify({
        type: Types.WARNING,
        message: 'Não foi possível carregar'
      });

    });
}

populateWithMyResults(myResults: any){
  let results = []
  results = this.meuAtivoProvider.setMeusResultados(myResults);

  results.forEach(element =>{
    element.paceMedio  =  element.paceMedio.substring(3)
  })


  this.myResultsToShow = results;
  console.log(this.myResultsToShow);

  if(!this.myResultsToShow.length){

    this.notifyEvent.setNotify({
      type: Types.NOTFOUND,
      message: 'Nenhum evento foi encontrado'
    });

  }else{
    this.notifyEvent.clear();

  }
}

goToResultDetail(result){

  let loading = this.loadingCtrl.create({
    content: 'Carregando...',
    cssClass: 'login-loading'
  });

  loading.present();

  //console.log(result)
  let eventResult: any;

  this.homePageProvider.getEventoPorId(result.idEvento)
    .subscribe(data=>{
      var detalhes = JSON.parse(data["_body"]);
      eventResult = this.homePageProvider.populateResultsEvents(JSON.parse(data["_body"]))[0]
      eventResult.dsRetiradaKits = detalhes[0].ds_retirada_kits;
      eventResult.regulamento = detalhes[0].ds_regulamento_txt;
      eventResult.percursos = this.addPercursos(detalhes[0].percursos);


      this.resultsProvider.getPositionsDetail(result.idEvento, result.numPeito)
        .subscribe(data=>{
        let myPosition = this.resultsProvider.populatePositionShowcase(JSON.parse(data["_body"]))[0];
        //console.log(myPosition)
        this.navCtrl.push(ResultadoAtletaDetalhesPage, {item: myPosition, evento: eventResult})
      },error=>{

      }, ()=>{
        loading.dismiss();
      })
    }, error =>{
      loading.dismiss();

    })

}

addPercursos(percursos: Array<any>){
  var percursosSerializados = new Array<Percurso>()

  for (let item of percursos){
    var serializado = new Percurso();
    serializado.modalidade = item.nm_modalidade;
    serializado.mapa = item.ds_mapa_google;
    percursosSerializados.push(serializado);
  }
  return percursosSerializados
}


menuToggle(){
  this.menuCtrl.toggle();
}

}
