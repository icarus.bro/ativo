import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  // hardcoded data to calendar, list of categories
  categories = [
    { name: 'Corrida de Rua', img: './assets/images/street-run.jpg', countEvents: 41 },
    { name: 'Travessia', img: './assets/images/swimming.jpg', countEvents: 28 },
    { name: 'Cross Training', img: './assets/images/cross-training-string.jpg', countEvents: 24 },
    { name: 'Corrida de Montanha', img: './assets/images/montain-run.jpg', countEvents: 14 },
    { name: 'Corrida de Obstáculos', img: './assets/images/obstacles-2.jpg', countEvents: 64 },
    { name: 'Corrida de Bike', img: './assets/images/bike.jpg', countEvents: 72 },
    { name: 'Outros', img: './assets/images/others.jpg', countEvents: 100 }
  ]

  constructor(public navCtrl: NavController) {

  }


  

}
