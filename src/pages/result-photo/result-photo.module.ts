import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ResultPhotoPage } from './result-photo';

@NgModule({
  declarations: [
    ResultPhotoPage,
  ],
  imports: [
    IonicPageModule.forChild(ResultPhotoPage),
  ],
})
export class ResultPhotoPageModule {}
