import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ResultPhotoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-result-photo',
  templateUrl: 'result-photo.html',
})
export class ResultPhotoPage {

  images

  constructor(public navCtrl: NavController, public navParams: NavParams) {

    this.images = [
      {img: './assets/images/street-run.jpg'},
      {img: './assets/images/bike.jpg'},
      {img: './assets/images/street-run.jpg'},
      {img: './assets/images/montain-run.jpg'},
      {img: './assets/images/street-run.jpg'},
      {img: './assets/images/street-run.jpg'},
      {img: './assets/images/street-run.jpg'},
      {img: './assets/images/street-run.jpg'},
      {img: './assets/images/swimming.jpg'},
      {img: './assets/images/street-run.jpg'},
      {img: './assets/images/swimming.jpg'},
      {img: './assets/images/street-run.jpg'},
      {img: './assets/images/street-run.jpg'},
      {img: './assets/images/montain-run.jpg'},
      {img: './assets/images/street-run.jpg'},
      {img: './assets/images/street-run.jpg'},
    ]

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ResultPhotoPage');
  }

}
