import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';
import { ActionSheet, ActionSheetOptions } from '@ionic-native/action-sheet';

@Component({
  selector: 'page-info-tips-read',
  templateUrl: 'info-tips-read.html',
})
export class InfoTipsReadPage {

  img: string
  title: string
  author: string
  date: string
  text: string
  article: any[]



  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public viewCtrl: ViewController,
              private actionSheet: ActionSheet,
              private socialSharing: SocialSharing) {


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InfoTipsReadPage')
    this.img = this.navParams.get('img')
    this.title = this.navParams.get('title')
    this.author = this.navParams.get('author')
    this.date = this.navParams.get('date')
    this.text = this.navParams.get('text')
    console.log( this.img )


  }

  dismiss() {
    this.viewCtrl.dismiss()
  }

  callActionSheet(){

        let buttonLabels = ['Compartilhar via Email', 'Compartilhar via Facebook', 'Compartilhar via WhatsApp', 'Compartilhar via Instagram'];

        const options: ActionSheetOptions = {
          title: 'Compartilhar?',
          subtitle: 'Escolha uma opção',
          buttonLabels: buttonLabels,
          addCancelButtonWithLabel: 'Cancelar',
          androidTheme: this.actionSheet.ANDROID_THEMES.THEME_HOLO_DARK,
          destructiveButtonLast: true
        };

        this.actionSheet.show(options).then((buttonIndex: number) => {
          // this.teste = buttonIndex.toString()
          console.log('Button pressed: ' + buttonIndex);

          if(buttonIndex == 1){
            this.shareEmail()
          }
          else if(buttonIndex == 2){
            this.shareFacebook()
          }else if(buttonIndex == 3){
            this.shareWhatsapp()
          }else if(buttonIndex == 4){
            this.shareInstagram()
          }
        });
      }

      share = [
        { name: 'Corrida de Rua', img: './assets/imgs/logo.png', link: "https://www.facebook.com/events/2047591082136451/" },
      ]

      shareFacebook(){

        this.socialSharing.shareViaFacebook(this.share[0].name, this.share[0].img, this.share[0].link)

      }

      shareInstagram(){
        this.socialSharing.shareViaInstagram(this.share[0].name, 'https://do1pouckcwxot.cloudfront.net/brasil/uploads/2017/11/06165137/20634742_322803388179585_6307485200200761344_n.jpg')
      }

      shareWhatsapp(){
        this.socialSharing.shareViaWhatsApp(this.share[0].name, this.share[0].img, this.share[0].link)
      }

      shareEmail(){
        this.socialSharing.canShareViaEmail().then(() => {
        }).catch(() => {
        });
        this.socialSharing.shareViaEmail('Mensagem', 'Assunto', ['dcarvalho@mobdrops.com.br'])
          .then(() => {
          }).catch(() => {
        });
      }
}
