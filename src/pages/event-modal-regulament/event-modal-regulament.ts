import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-event-modal-regulament',
  templateUrl: 'event-modal-regulament.html',
})
export class EventModalRegulamentPage {

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EventModalRegulamentPage');
  }

  dismiss() {
    this.viewCtrl.dismiss()
  }

}
