import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EventModalRegulamentPage } from './event-modal-regulament';

@NgModule({
  declarations: [
    EventModalRegulamentPage,
  ],
  imports: [
    IonicPageModule.forChild(EventModalRegulamentPage),
  ],
})
export class EventModalRegulamentPageModule {}
