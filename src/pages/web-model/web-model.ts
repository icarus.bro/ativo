import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';

/**
 * Generated class for the WebModelPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-web-model',
  templateUrl: 'web-model.html',
})
export class WebModelPage {
  url: string;
  title: any
  back: any;
  safeUrl: any;

  constructor(public viewCtrl: ViewController, 
              public navParams: NavParams,
              public domSanitizer: DomSanitizer) {  }

  public ngOnInit() {

    let url = this.navParams.get('url');
    this.title = this.navParams.get('title');    

    if(url.search('http')){
      this.safeUrl = this.getSafeURL('http://' + this.navParams.get('url'));
    }else{
      this.safeUrl = this.getSafeURL(this.navParams.get('url'));
    }
  }

  public getSafeURL(url: string) {
    return this.domSanitizer.bypassSecurityTrustResourceUrl(url);
  }

  public dismiss() {
    this.viewCtrl.dismiss();
  }

}
