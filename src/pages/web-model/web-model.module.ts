import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WebModelPage } from './web-model';

@NgModule({
  declarations: [
    WebModelPage,
  ],
  imports: [
    IonicPageModule.forChild(WebModelPage),
  ],
})
export class WebModelPageModule {}
