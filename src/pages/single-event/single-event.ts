import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, MenuController } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';
import { DomSanitizer } from '@angular/platform-browser';
import { Percurso } from '../../models/resultado-evento-model';
import { WebModelPage } from '../web-model/web-model';
import { HomePageProvider } from '../../providers/home-page/home-page';
import { Content } from 'ionic-angular';

@Component({
  selector: 'page-single-event',
  templateUrl: 'single-event.html',
})
export class SingleEventPage {

  @ViewChild(Content) content: Content;

    event: any;
    id: any;
    regulamento: any;
    dates:any;
    today = new Date();
    kits: any;
    percurso: any;

    public percursos: Array<Percurso>
    public percursoSelecionado: string;
    public urlSanitizada: any;

    segmentOptionSelected: string = 'informacoes'

    constructor(private navCtrl: NavController,
                private navParams: NavParams,
                private socialSharing: SocialSharing,
                private homePageProvider: HomePageProvider,
                public sanitizer: DomSanitizer,
                public menuCtrl: MenuController) {

      this.dates = {};
      this.kits = [];
      this.percurso = [];
      this.percursos = [];
      this.event = {};

    }

    ionViewDidLoad(){
      this.initialize()
    }

    public initialize() {
      this.event = this.navParams.get('event');
      this.id = this.navParams.get('id');

      if(this.event.ds_site && this.event.ds_site.search('http')){
        this.event.ds_site = 'http://' + this.event.ds_site;
      }

      this.homePageProvider.getEventoPorId(this.id)
        .subscribe(data=>{
          var detalhes = JSON.parse(data["_body"]);
          console.log(detalhes)
          this.regulamento = detalhes[0].ds_regulamento_txt;
          this.dates = {
            start: new Date(detalhes[0].dt_inicio_inscricao.split(" ")[0]+"T00:00:01"),
            end: new Date(detalhes[0].dt_fim_inscricao.split(" ")[0]+"T23:59:59")
          }
          this.kits = detalhes[0].kits;
          this.percursos = this.addPercursos(detalhes[0].percursos);
          this.percursoSelecionado = this.percursos[0].modalidade
          this.urlSanitizada = this.sanitizer.bypassSecurityTrustResourceUrl("http://"+this.percursos[0].mapa);
          //console.log(this.percurso);

          //console.log("agora " + this.today +" data fim "+this.dates.end)


          if(this.today > this.dates.start && this.today < this.dates.end){
            this.content.resize();
          }
        }, error =>{
          console.log('deu ruim para consumir evento pelo id');
        })

    }

    addPercursos(percursos: Array<any>){
      var percursosSerializados = new Array<Percurso>()

      for (let item of percursos){
        var serializado = new Percurso();
        serializado.modalidade = item.nm_modalidade;
        serializado.mapa = item.ds_mapa_google;
        //console.log(serializado);
        percursosSerializados.push(serializado);
      }
      return percursosSerializados
    }

    goToRegulamento(){
      let item = {regulamento: this.regulamento}
      // this.navCtrl.push(RegulamentoPage,item);
    }

    goToWebModel(){
      this.navCtrl.push(WebModelPage, {url: "http://checkout.ativo.com/evento/"+this.id, title: "Inscrições"})
    }

    public doShare() {
      console.log(this.event);
      this.socialSharing.share(this.event.ds_evento, this.event.description, null, this.event.ds_site);

    }

    limpaUrl(mapa){
      this.urlSanitizada = this.sanitizer.bypassSecurityTrustResourceUrl("http://"+mapa);
    }

    recalculatingFooter(){
      let timeoutId = setTimeout(() => {
        console.log(this.urlSanitizada)
        this.content.resize();
      }, 100);

      timeoutId;

    }

    menuToggle(){
      this.menuCtrl.toggle();
    }
  }

