import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AppAvailability } from '@ionic-native/app-availability';
import { Device } from '@ionic-native/device';
import { InAppBrowser } from '@ionic-native/in-app-browser';

/**
 * Generated class for the FeedSpotifyPlaylistsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-feed-spotify-playlists',
  templateUrl: 'feed-spotify-playlists.html',
  providers: [AppAvailability, Device, InAppBrowser ]
})
export class FeedSpotifyPlaylistsPage {

  // hardcoded data to calendar, list of categories
  playslists = [
    { name: 'Playlist 1', img: './assets/images/street-run.jpg'},
    { name: 'Playlist 2', img: './assets/images/swimming.jpg'},
    { name: 'Playlist 3', img: './assets/images/cross-training-string.jpg'},
    { name: 'Playlist 4', img: './assets/images/montain-run.jpg'},
    { name: 'Playlist 5', img: './assets/images/obstacles-2.jpg'},
  ]

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private appAvailability: AppAvailability,
    private device: Device,
    private inAppBrowser: InAppBrowser) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeedSpotifyPlaylistsPage');
  }

  appAvailabity(iosSchemaName: string, androidPackageName: string, appUrl: string, httpUrl: string) {
    let app: string;
    if (this.device.platform === 'iOS') {
      app = iosSchemaName;
    } else if (this.device.platform === 'Android') {
      app = androidPackageName;
    } else {
      let browser = this.inAppBrowser.create(httpUrl, '_system');
      return;
    }

    this.appAvailability.check(app).then(
      () => { // success callback
        let browser = this.inAppBrowser.create(httpUrl, '_system');
      },
      () => { // error callback
        let browser = this.inAppBrowser.create(httpUrl, '_system');
      }
    );
  }

  openSpotify() {
    this.appAvailabity('(Spotify://', 'com.spotify.sdk', 'com.spotify.sdk:spotify-player:', 'spotify:track:');
    console.log("------ Spotify --------");
  }

  //---> OK para verificar disponibilidade
  // appExistStatus: any;

  // checkAv(iosSchemaName: string){
  //   console.log("ok");
  //   let app;
  //   if (this.platform.is('ios')){
  //     app = 'spotify://';
  //   }else if(this.platform.is('android')){
  //     app = "com.spotify.android";
  //   }
  //   console.log(app);
  //   this.appAvailability.check(app)
  //     .then(
  //       (yes) => {
  //         console.log(app + 'is avai');
  //         this.appExistStatus = app + ' is ava';
  //       },
  //       (no) => {
  //         console.log(app + 'NOT avai');
  //         this.appExistStatus = app + ' NOT ava';
  //       }
  //     );
  // }

}
