import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { InfoNewsReadPage } from '../info-news-read/info-news-read';

@Component({
  selector: 'page-info-news',
  templateUrl: 'info-news.html',
})
export class InfoNewsPage {

   page: any = InfoNewsReadPage

   // hardcoded data to calendar, list of categories
   news = [
    { title: 'Saúde', brief: 'Mussum Ipsum, cacilds vidis litro abertis. Per aumento de cachacis, eu reclamis.', img: 'https://ionicframework.com/dist/preview-app/www/assets/img/thumbnail-totoro.png', flag: 0 },
    { title: 'Nutrição', brief: 'Mussum Ipsum, cacilds vidis litro abertis. Per aumento de cachacis, eu reclamis.', img: 'https://ionicframework.com/dist/preview-app/www/assets/img/thumbnail-rotla.png', imgFeatured: 'https://dspncdn.com/a1/media/692x/52/1a/fd/521afd2ba19f10e2652c39e42c53e9d1.jpg', flag: 1  },
    { title: 'Mulher', brief: 'Mussum Ipsum, cacilds vidis litro abertis. Per aumento de cachacis, eu reclamis.', img: 'https://ionicframework.com/dist/preview-app/www/assets/img/thumbnail-ghostbusters.png', flag: 0 },
    { title: 'Experts', brief: 'Mussum Ipsum, cacilds vidis litro abertis. Per aumento de cachacis, eu reclamis.', img: 'https://ionicframework.com/dist/preview-app/www/assets/img/thumbnail-bttf.png', flag: 0 },
    { title: 'Repórter Ativo', brief: 'Mussum Ipsum, cacilds vidis litro abertis. Per aumento de cachacis, eu reclamis.', img: 'https://ionicframework.com/dist/preview-app/www/assets/img/thumbnail-terminator.png', imgFeatured: 'https://dspncdn.com/a1/media/692x/e9/79/8f/e9798f4004f53b6c872de2e0cdf3f39f.jpg', flag: 1 },
  ]

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InfoNewsPage');
  }


  openNewsForRead(){
    this.navCtrl.push(this.page)
  }

}
