import { Component } from '@angular/core';
import { InfoNewsPage } from '../info-news/info-news';
import { InfoTipsPage } from '../info-tips/info-tips';

@Component({
  selector: 'page-tabs-news-tips',
  templateUrl: 'tabs-news-tips.html',
})
export class TabsNewsTipsPage {

  tabNews: any
  tabTips: any

  constructor() {
    this.tabNews = InfoNewsPage
    this.tabTips = InfoTipsPage
  }

}
