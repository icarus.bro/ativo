import { Geolocation, Geoposition } from '@ionic-native/geolocation';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult } from '@ionic-native/native-geocoder';
import { Platform } from 'ionic-angular/platform/platform';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
@Injectable()
export class Util {

  location: any
  currentCoords: any
  locate: any

  constructor(public platform: Platform,
              public geolocation: Geolocation,
              public nativeGeocoder: NativeGeocoder) {


  }

   /**
   * Util to get current geolocation
   * Used in geolocation filter
   * @param
   */
    getCoordinates() {

      let options = {
        enableHighAccuracy: true,
        geolocate : true,
        accuracy: 4
      }

      this.geolocation.getCurrentPosition(options).then((position: Geoposition) => {})
    }

    /**
     * Function to convert lat/long to name
     * locate of type string.
     * @param object position
     *
     */
    convertCoordsToLocate() {

      // this.nativeGeocoder.reverseGeocode(position.coords.latitude, position.coords.longitude).then((result: NativeGeocoderReverseResult) => {

      //   this.locate = result.administrativeArea

      // }).catch((error) => {
      //   console.log( 'Error to convert coords.' )
      // })
    }

  public removeAccents(newStringComAcento) {

    var string = newStringComAcento;
    var mapaAcentosHex   = {
      a : /[\xE0-\xE6]/g,
      A : /[\xC0-\xC6]/g,
      e : /[\xE8-\xEB]/g,
      E : /[\xC8-\xCB]/g,
      i : /[\xEC-\xEF]/g,
      I : /[\xCC-\xCF]/g,
      o : /[\xF2-\xF6]/g,
      O : /[\xD2-\xD6]/g,
      u : /[\xF9-\xFC]/g,
      U : /[\xD9-\xDC]/g,
      c : /\xE7/g,
      C : /\xC7/g,
      n : /\xF1/g,
      N : /\xD1/g
    };

    for ( var letra in mapaAcentosHex ) {
      var expressaoRegular = mapaAcentosHex[letra];
      string = string.replace( expressaoRegular, letra );
    }

    return string;
  }

  public getClearCityName(city: string) {
      return this.removeAccents(city).toLowerCase().split(' ').join('_');
  }

  //http://www.receita.fazenda.gov.br/aplicacoes/atcta/cpf/funcoes.js
  //função copiada da receita federal
  public testCPF(strCPF: string): boolean{
    var Soma;
    var Resto;
    Soma = 0;
    if (strCPF == "00000000000")
	    return false;
    for (var i=1; i<=9; i++)
	    Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (11 - i);

    Resto = (Soma * 10) % 11;
    if ((Resto == 10) || (Resto == 11))
	    Resto = 0;

    if (Resto != parseInt(strCPF.substring(9, 10)) )
	    return false;

	  Soma = 0;
    for (var i = 1; i <= 10; i++)
       Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (12 - i);

    Resto = (Soma * 10) % 11;
    if ((Resto == 10) || (Resto == 11))
	    Resto = 0;

    if (Resto != parseInt(strCPF.substring(10, 11) ) )
      return false;

    return true;
  }

  public cleanCPF(cpf: string): string{
    //console.log("entrada "+ cpf)

    if(cpf.includes(".")){
      let d = cpf.split(".")
      //console.log(d)
      cpf = ""
      d.forEach(Element =>{
        cpf = cpf+Element
      })
    }
    if(cpf.includes("-")){
      let d = cpf.split("-")
      cpf = d[0]+d[1];
    }

    //console.log("saida "+ cpf)
    return cpf;
  }
}




