import { Http } from '@angular/http';
import { Geolocation, Geoposition } from '@ionic-native/geolocation';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult } from '@ionic-native/native-geocoder';
import { Injectable } from '@angular/core';
@Injectable()
export class Localization {

  constructor(private http: Http,
              private geolocation: Geolocation,
              private nativeGeocoder: NativeGeocoder){

              }
    /**
     * First the function get coordinates position, after convert
     * coords to locate
     * @param
     */
  getCurrentCoordinates(): Promise<NativeGeocoderReverseResult> {
    return new Promise((resolve, reject) => {
      let options = {
        enableHighAccuracy: true,
        geolocate : true,
        accuracy: 4
      }

      this.geolocation.getCurrentPosition().then((position) => {
        resolve(position)
      }).catch((error) => {
        reject(error)
      })
    })
    .then((position: Geoposition) => {
      return this.nativeGeocoder.reverseGeocode(position.coords.latitude, position.coords.longitude)
    })
  }

  /**
   * List all locate to events
   * @param
   */
  getAllLocates(): Promise<Response> {
    return new Promise((resolve, reject) => {
      let url = "http://webservices.esferabr.com.br/Mobile/Cidade"

      this.http.get(url).subscribe((result: any) => {
        console.log(result)
        resolve(result.json())
      },
      (error) => {
        reject(error.json())
      })
    })
  }
}
