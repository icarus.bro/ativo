import { NavController, ModalController } from 'ionic-angular';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'event-component-item',
  templateUrl: 'event-component-item.html'
})
export class EventComponentItemComponent {

  @Input('id')
  id: string

  @Input('name')
  name: string

  @Input('img')
  img: string

  @Input('local')
  local: string

  @Input('cidade')
  cidade: string

  @Input('data')
  data: string

  @Input('event')
  event: any

  @Input('page')
  page: any

  constructor(private navCtrl: NavController,
              public modalCtrl: ModalController) {


  }

  showEventResults() {
    this.navCtrl.push(this.page, {
      id: this.id,
      event: this.event
    })
  }

}
