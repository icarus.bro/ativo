import { Component, Input } from '@angular/core';

/**
 * Generated class for the ReceiptAthleteItemComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'receipt-athlete-item',
  templateUrl: 'receipt-athlete-item.html'
})
export class ReceiptAthleteItemComponent {

  text: string;

  constructor() {
    console.log('Hello ReceiptAthleteItemComponent Component');
    this.text = 'Hello World';
  }

  @Input('athlete')
  athlete


}
