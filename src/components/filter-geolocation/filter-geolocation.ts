import { Localization } from './../../util/localization';
import { ModalController } from 'ionic-angular';
import { Component, Input } from '@angular/core';
import { EventModalGeolocationCategoryPage } from '../../pages/event-modal-geolocation-category/event-modal-geolocation-category';

@Component({
  selector: 'filter-geolocation',
  templateUrl: 'filter-geolocation.html'
})
export class FilterGeolocationComponent {

  text: string;
  modalGeolocationFilterPage = EventModalGeolocationCategoryPage
  locates: any[]
  @Input() public eventCity: string
  locate: string = ''

  constructor(public modalCtrl: ModalController,
              private localization: Localization,) {

                this.getAllLocatesToFilter()

  }

  /**
   * Open modal to select a new locate
   * @param locate
   *
   */
  openFilterByGeolocation(){
    let searchLocate = this.modalCtrl.create(this.modalGeolocationFilterPage)
    searchLocate.present()
  }

  /**
   * List all locates to
   */
  getAllLocatesToFilter() {
    this.localization.getAllLocates()
      .then((response: any) => {
        this.locates = response
      }).catch((error) => {
        console.log( 'Não foi possível carregar cidades.' )
      })
  }

}
