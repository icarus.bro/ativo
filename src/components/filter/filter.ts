import { Localization } from './../../util/localization';
import { Component } from '@angular/core';
import { NativeGeocoderReverseResult } from '@ionic-native/native-geocoder';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { EventModalGeolocationCategoryPage } from '../../pages/event-modal-geolocation-category/event-modal-geolocation-category';
import { NavParams } from 'ionic-angular/navigation/nav-params';

@Component({
  selector: 'filter',
  templateUrl: 'filter.html'
})
export class FilterComponent {

  text: string;
  currentGeolocation: string
  modalGeolocationFilterPage = EventModalGeolocationCategoryPage
  geolocation = 'Hard Geo'

  constructor(private localization: Localization,
              public modalCtrl: ModalController,
              public navParams: NavParams) {

  }

  /**
     * Get current geolocation to filter events
     * @param
     */
    getCurrentGeolocation() {
      this.localization.getCurrentCoordinates().then((response: NativeGeocoderReverseResult) => {
        this.currentGeolocation = response.administrativeArea
        console.log( response.administrativeArea )
      }).catch((error) => {
        console.log( 'Não foi possível capturar a localização.' + error)
      })
    }

    /**
     * Modal to change filter
     * events
     */
    openFilterGeolocation() {
      let modal = this.modalCtrl.create(this.modalGeolocationFilterPage)
      modal.present()
    }

}
