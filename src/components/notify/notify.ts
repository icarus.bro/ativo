import { Component, Input, EventEmitter, Output, OnInit } from '@angular/core';

export enum Types {
    WARNING = 1,
    LOADING,
    NOTFOUND
}

@Component({
  selector: 'notify',
  templateUrl: 'notify.html'
})
export class NotifyComponent implements OnInit {

  @Input('offline')
  offline;

  private LOADING  = Types.LOADING; 
  private WARNING  = Types.WARNING; 
  private NOTFOUND = Types.NOTFOUND;

  private message: string;
  public type: Types;

  @Output('callback')
  private callback: EventEmitter<any> = new EventEmitter<any>();

  public ngOnInit() {
    this.LOADING = Types.LOADING;
    this.WARNING = Types.WARNING;
    this.NOTFOUND = Types.NOTFOUND;
  }

  private call(){
    this.callback.emit();
  }

  public setNotify(payload: any){

    this.type = payload.type;
    payload.message ? this.message = payload.message : this.message = null;

  }

  public clear() {
    this.message = null;
    this.type = null;
    this.callback = null;
  }

}
