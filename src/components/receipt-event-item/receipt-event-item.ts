import { Component, Input } from '@angular/core';

/**
 * Generated class for the ReceiptEventItemComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'receipt-event-item',
  templateUrl: 'receipt-event-item.html'
})
export class ReceiptEventItemComponent {

  text: string;

  constructor() {
    console.log('Hello ReceiptEventItemComponent Component');
    this.text = 'Hello World';
  }

  @Input('event')
  event

}
