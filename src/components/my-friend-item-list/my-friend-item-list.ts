import { Component, Input } from '@angular/core';
import { UserLoginProvider } from '../../providers/user-login/user-login';
import { Events } from 'ionic-angular';
import { MyFriendsProvider } from '../../providers/my-friends/my-friends';

/**
 * Generated class for the MyFriendItemListComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'my-friend-item-list',
  templateUrl: 'my-friend-item-list.html'
})
export class MyFriendItemListComponent {

  text: string;

  // constructor() {
  //   console.log('Hello MyFriendItemListComponent Component');
  //   this.text = 'Hello World';
  // }

  @Input("name")
  name

  @Input("email")
  email

  @Input("idUser")
  idUser

  @Input("edit")
  edit = false;

  constructor( private friendProvider: MyFriendsProvider, private user: UserLoginProvider, private events: Events) {}

  deleteFriend(friendId){
    this.events.publish("attfriend", friendId);
  }


}
