import { ResultAthleteDetailsPage } from './../../pages/result-athlete-details/result-athlete-details';
import { ModalController } from 'ionic-angular';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'result-position-race-item',
  templateUrl: 'result-position-race-item.html'
})
export class ResultPositionRaceItemComponent {

  text: string;
  modalResultAthleteDetails = ResultAthleteDetailsPage

  @Input()
  classificacaoTotal: string

  @Input()
  tempoTotal: string

  @Input()
  nome: string

  @Input()
  numPeito: string

  @Input()
  position: any

  constructor(public modalCtrl: ModalController) {
    console.log('Hello ResultPositionRaceItemComponent Component');
    this.text = 'Hello World';
  }

   /**
   * Opens modal with athlete details
   * @param idAthlete
   *
   *
   */
  openResultDetails() {
    let modal = this.modalCtrl.create(this.modalResultAthleteDetails, {
      ranking: this.position
    })
    modal.present()
  }



}
