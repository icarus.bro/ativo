import { Component, Input } from '@angular/core';

/**
 * Generated class for the KitToBuyListComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'kit-to-buy-list',
  templateUrl: 'kit-to-buy-list.html'
})
export class KitToBuyListComponent {

  text: string;

  @Input('kit')
  kit;

  constructor() {
    console.log(this.kit+"");
  }


}
