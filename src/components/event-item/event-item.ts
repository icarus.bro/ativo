import { Component, Input } from '@angular/core';
import { NavController } from 'ionic-angular'

@Component({
  selector: 'event-item',
  templateUrl: 'event-item.html'
})
export class EventItem {

  constructor(private navCtrl: NavController) { }

  @Input('name')
  eventName: string;
  
  @Input('page')
  page: any;

  @Input('date')
  eventDate: any;
  
  @Input('locale')
  eventLocale: string;

  @Input('img')
  eventImgUrl: string;

  @Input('id')
  id: string;

  @Input('event')
  event: string;

  public doClick() {
    console.log( this.page )
    this.navCtrl.push(this.page, {event: this.event, id: this.id});
  }

}
