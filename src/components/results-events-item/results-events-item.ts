import { Component, Input } from '@angular/core';

/**
 * Generated class for the ResultsEventsItemComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'results-events-item',
  templateUrl: 'results-events-item.html'
})
export class ResultsEventsItemComponent {

  text: string;

  @Input('id')
  id: string

  @Input('evento')
  evento: string

  @Input('cidade')
  cidade: string

  @Input('estado')
  estado: string

  @Input('dataEvento')
  dataEvento: string

  @Input('tempoTotal')
  tempoTotal: string

  @Input('paceMedio')
  paceMedio: string

  @Input('posicao')
  posicao: string

  @Input('result')
  result: any

  constructor() {
    console.log('Hello ResultsEventsItemComponent Component');
    this.text = 'Hello World';
  }

}
