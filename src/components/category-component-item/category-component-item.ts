import { NavController } from 'ionic-angular';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'category-component-item',
  templateUrl: 'category-component-item.html'
})
export class CategoryComponentItemComponent {

  @Input('id')
  id: string

  @Input('name')
  name: string

  @Input('page')
  page: any

  @Input('teste')
  teste : any

  @Input('geolocation')
  geolocation: string


  constructor(private navCtrl: NavController) {

  }

  public listEventsCategory() {
    this.navCtrl.push(this.page, {
      id: this.id,
      name: this.name,
      teste: this.teste,
      geolocation: this.geolocation
    });
  }

}
