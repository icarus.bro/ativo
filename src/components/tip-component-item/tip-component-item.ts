import { Component, Input } from '@angular/core';

@Component({
  selector: 'tip-component-item',
  templateUrl: 'tip-component-item.html'
})
export class TipComponentItemComponent {

  text: string;

  @Input('id')
  id: string

  @Input('name')
  name: string

  @Input('img')
  img: string

  @Input('page')
  page: any


  constructor() {
    console.log('Hello TipComponentItemComponent Component');
    this.text = 'Hello World';
  }

}
