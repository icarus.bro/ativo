import { Component, Input } from '@angular/core';

/**
 * Generated class for the KitItemListComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'kit-item-list',
  templateUrl: 'kit-item-list.html'
})
export class KitItemListComponent {

  text: string;

  constructor() {
    console.log('Hello KitItemListComponent Component');
    this.text = 'Hello World';
  }

  @Input('eventName')
  eventName: string;

  @Input('dateEvent')
  dateEvent: string;

  @Input('adress')
  adress: string

  // @Input('imagem')
  // imagem: string

  @Input('local')
  local: string;

  @Input('receipt')
  receipt: string;

}
