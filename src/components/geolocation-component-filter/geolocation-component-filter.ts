import { Util } from './../../util/util';
import { NavController, NavParams } from 'ionic-angular';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'geolocation-component-filter',
  templateUrl: 'geolocation-component-filter.html'
})
export class GeolocationComponentFilterComponent implements OnInit {

  @Input('location')
  location: any

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private util: Util) {

  }

  ngOnInit() {
    this.getMyLocation()
  }

  getMyLocation() {
    this.util.getCoordinates()
  }

}
