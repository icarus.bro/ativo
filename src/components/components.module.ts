import { NgModule } from '@angular/core';
import { CategoryComponentItemComponent } from './category-component-item/category-component-item';
import { EventComponentItemComponent } from './event-component-item/event-component-item';
import { TipComponentItemComponent } from './tip-component-item/tip-component-item';
import { EditorialComponentItemComponent } from './editorial-component-item/editorial-component-item';
import { NotifyComponent } from './notify/notify';
import { EventItem } from './event-item/event-item';
import { ReceiptEventItemComponent } from './receipt-event-item/receipt-event-item';
import { ReceiptAthleteItemComponent } from './receipt-athlete-item/receipt-athlete-item';
import { ReceiptProductItemComponent } from './receipt-product-item/receipt-product-item';
import { KitItemListComponent } from './kit-item-list/kit-item-list';
import { KitToBuyListComponent } from './kit-to-buy-list/kit-to-buy-list';
import { GeolocationComponentFilterComponent } from './geolocation-component-filter/geolocation-component-filter';
import { MyFriendItemListComponent } from './my-friend-item-list/my-friend-item-list';
import { ResultsEventsItemComponent } from './results-events-item/results-events-item';
import { ResultDetailComponent } from './result-detail/result-detail';
import { ResultPositionRaceItemComponent } from './result-position-race-item/result-position-race-item';
import { FilterComponent } from './filter/filter';
import { FilterGeolocationComponent } from './filter-geolocation/filter-geolocation';

@NgModule({
	declarations: [CategoryComponentItemComponent,
    EventComponentItemComponent,
    TipComponentItemComponent,
    EditorialComponentItemComponent,
    NotifyComponent,
    EventItem,
    ReceiptEventItemComponent,
    ReceiptAthleteItemComponent,
    ReceiptProductItemComponent,
    KitItemListComponent,
    KitToBuyListComponent,
    GeolocationComponentFilterComponent,
    MyFriendItemListComponent,
    ResultsEventsItemComponent,
    ResultDetailComponent,
    ResultPositionRaceItemComponent,
    FilterComponent,
    FilterGeolocationComponent,
    FilterGeolocationComponent,

    ],
	imports: [],
	exports: [CategoryComponentItemComponent,
    EventComponentItemComponent,
    TipComponentItemComponent,
    EditorialComponentItemComponent,
    NotifyComponent,
    EventItem,
    ReceiptEventItemComponent,
    ReceiptAthleteItemComponent,
    ReceiptProductItemComponent,
    KitItemListComponent,
    KitToBuyListComponent,
    GeolocationComponentFilterComponent,
    MyFriendItemListComponent,
    ResultsEventsItemComponent,
    ResultDetailComponent,
    ResultPositionRaceItemComponent,
    FilterComponent,
    FilterGeolocationComponent,
    FilterGeolocationComponent,

    ]
})
export class ComponentsModule {}
