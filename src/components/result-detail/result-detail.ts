import { Component } from '@angular/core';

@Component({
  selector: 'result-detail',
  templateUrl: 'result-detail.html'
})
export class ResultDetailComponent {

  text: string;

  constructor() {
    console.log('Hello ResultDetailComponent Component');
    this.text = 'Hello World';
  }

}
