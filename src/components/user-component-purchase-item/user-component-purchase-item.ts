import { UserPurchaseDetailPage } from './../../pages/user-purchase-detail/user-purchase-detail';
import { ModalController, NavController } from 'ionic-angular';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'user-component-purchase-item',
  templateUrl: 'user-component-purchase-item.html'
})
export class UserComponentPurchaseItemComponent {

  @Input('purchaseName')
  purchaseName: string

  @Input('cosumerName')
  cosumerName: string

  @Input('payment')
  payment: string

  @Input('status')
  status: string

  @Input('price')
  price: number

  @Input('taxService')
  taxService: string

  @Input('raceCategory')
  raceCategory: string

  @Input('inscriptionLevel')
  inscriptionLevel: string

  @Input('kit')
  kit: boolean

  @Input('purchase')
  purchase: any

  constructor(public modalCtrl: ModalController,
              public navCtrl: NavController) {

  }

  openTicketPurchase() {
    this.navCtrl.push(UserPurchaseDetailPage, {
      purchase: this.purchase
    })
  }
}
