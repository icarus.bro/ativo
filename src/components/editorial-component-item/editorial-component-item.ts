import { ModalController } from 'ionic-angular';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'editorial-component-item',
  templateUrl: 'editorial-component-item.html'
})
export class EditorialComponentItemComponent {

  @Input('id')
  id: string

  @Input('img')
  img: string

  @Input('title')
  title: string

  @Input('author')
  author: string

  @Input('date')
  date: string

  @Input('text')
  text: string

  @Input('article')
  article: any

  @Input('page')
  page: any

  constructor(public modalCtrl: ModalController) {
    console.log('Hello EditorialComponentItemComponent Component');
    this.text = 'Hello World';
  }

  openTip() {
    let modal = this.modalCtrl.create(this.page, {
      img: this.img,
      title: this.title,
      author: this.author,
      date: this.date,
      text: this.text,
      article: this.article
    })
    modal.present()
  }


}
