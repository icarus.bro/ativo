import { Component, Input } from '@angular/core';

/**
 * Generated class for the ReceiptProductItemComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'receipt-product-item',
  templateUrl: 'receipt-product-item.html'
})
export class ReceiptProductItemComponent {

  text: string;

  constructor() {
    console.log('Hello ReceiptProductItemComponent Component');
    this.text = 'Hello World';
  }

  @Input('product')
  product: any;

}
