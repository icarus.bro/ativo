import { Injectable } from '@angular/core';

declare var navigator;

@Injectable()
export class NetworkService {

  online: boolean = true;

  constructor() {

    setInterval(() => {
      this.online = this.isOnline();
    }, 1000);

  }

  private isOnline():boolean {
    return navigator.onLine;
  }

}
