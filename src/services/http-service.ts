import { Injectable } from '@angular/core';
import { Http, RequestOptions, RequestOptionsArgs, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

/*
  Class the Http Service.
  
*/
enum HTTPMethods {
    GET,
    POST,
    PUT,
    DELETE
}

@Injectable()
export class HttpService {

    constructor(private http: Http) { }

    get(url: string, options?: RequestOptionsArgs): Observable<Response> {
        return this.intercept(this.http.get(url, this.getRequestOptionArgs(options)), url, HTTPMethods.GET);
    }

    post(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {   
        return this.intercept(this.http.post(url, body, this.getRequestOptionArgs(options)), url, HTTPMethods.POST);
    }

    put(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
        return this.intercept(this.http.put(url, body, this.getRequestOptionArgs(options)), url, HTTPMethods.PUT);
    }

    delete(url: string, options?: RequestOptionsArgs): Observable<Response> {
        return this.intercept(this.http.delete(url, this.getRequestOptionArgs(options)), url, HTTPMethods.DELETE);
    }
    
    getRequestOptionArgs(options?: RequestOptionsArgs) : RequestOptionsArgs {
      if (options == null) {
          options = new RequestOptions();
      }

      if (options.headers == null) {
          options.headers = new Headers();
      }

    //   options.headers.append('Content-Type', 'application/json');

      return options;
    }

    intercept(observable: Observable<Response>, url: string, method: HTTPMethods): Observable<any> {
        // let self = this;

        return Observable.create(observer => {
            observable.subscribe(res => {

                // if(res.ok && method == HTTPMethods.GET){
                //     self.cash.cash(res, url);
                // }

                observer.next(res);
                observer.complete();

            }, err => {

                // let selfErr = err;

                // if(method == HTTPMethods.GET){

                //     self.cash.getCash(url)
                //         .then(res => {

                //             if(res){
                //                 observer.next({
                //                     json: ()=>{
                //                         return JSON.parse(res)
                //                     }
                //                 });
                //                 observer.complete();

                //             }else{
                //                 observer.error(err);
                //                 observer.complete();

                //             }
                        
                //         }).catch(err => {
                //             observer.error(selfErr);
                //             observer.complete();
                //         });

                // }else{
                    observer.error(err);
                    observer.complete();
                // }
                
            });
        });
    }


}
