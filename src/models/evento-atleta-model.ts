export class EventoAtleta{
    idPedido: number;
    dataPedido = new Date();
    idUsuario: number;
    nome: string;
    pelotao: string;
    modalidade: string;
    categoria: string;
    equipeRevezamento: string;
    equipe: string;
    precoEvento: number;
    taxaEvento: number;
    taxaDelivery: number;
    quantidadeEvento: number;
    numeroQuantidade: number;
    tamanhoCamiseta: string;
    numeroPeito:number;
    idEvento:number;
    nomeEvento:string;
    dataEvento = new Date();
    enderecoEvento: string;
    etapa:string;
}