
export class FotoResultadoModel {
    idEventoCompreFotoItem: number;
    idEventoCompreFoto: number;
    nrPeito: number;
    dsAtleta: string;
    dsEquipe: string;
    idEvento: number;
    qtd: number;
    dsFoto: string;
    etapa: string;
    etapa2: string;
    sexo: string;
    horario: string;
    dtCadastro = new Date();
    flFotoNome: number;

}
