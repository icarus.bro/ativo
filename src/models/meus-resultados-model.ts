
export class MeusResultadoModel{

    categoria: string;
    certificado: string;
    cidade: string;
    estado:string;
    evento: string;
    local: string;
    modalidade: string;
    urlImagem = new Image();
    dataEvento = new Date();
    equipe: string;
    idEvento: number;
    idResultado: number;
    paceMedio: string;
    nome: string;
    numPeito: number;
    sexo: string;
    tempoTotal: string;
    posicao: number;

}

export class Image{
    banner: string;
    logo: string;
}

