export class Pagamento{
    id:number;
    valor:number;
    valorPago:number;
    formaPagamento:string;
    pagamentoStatus:string;
}