import { Image } from './meus-resultados-model';
import { ModalidadeModel } from './modalidade-model';


export class ResultadoEventoModel{
    
    idEvento: number;
    dsEvento : string;
    dsEventoES : string;
    dsEventoEN : string;
    dsDescricaoEventoEN : string;
    dsDescricaoEventoES : string;
    descriptionEn : string;
    descriptionEs : string;
    dsYoutube : string;
    dsDescricaoEventoResumido : string;
    dtEvento = new Date();
    dsTipoData : string;
    idCircuito :  number;
    dsCircuito : string;
    dtInicioInscricao = new Date();
    dtFimInscricao = new Date();
    hrEvento = new Date();
    flCarrossel :  number;
    dsEndereco : string;
    dsBairro : string;
    keywords : string;
    description : string;
    dsCidade : string;
    idContinente : number;
    dsContinente : string;
    dsPais : string;
    dsSigla : string;
    dsEstado : string;
    dsEstadoNome : string;
    dsSite : string;
    dsLocal : string;
    flEncerrarInscricao : number;
    flResultado: number;
    flHome : number;
    idTipoEvento : number;
    dsTipoEvento : string;
    hashtagInstagram : string;
    flEventoDaCasa : number;
    flFoto : number;
    flEventoSimples :  number;
    flFotoNome : number;
    dsInfoGrupo : string;
    dataUpdate = new Date();
    idEventoSituacao : number;
    idSituacaoCadastro : number;
    flViradaPorLote : number;
    idPaisOrigem : number;
    horaInicioCorrida = new Date();
    horaFimCorrida = new Date();
    surveyMonkey = new Date();
    nmCertificadoEvento: string;
    idCity :  number ;
    cityBr : string;
    cityEn : string;
    cityFr : string;
    cityDe : string;
    cityEs : string;
    countryBr : string;
    countryEn : string;
    countryFr : string;
    countryDe : string;
    countryEs : string;
    stateBr : string;
    stateEn : string;
    stateFr : string;
    stateDe : string;
    stateEs : string;
    dsLinkFaq : string;
    fotoClube: number;
    insOpen :  number;
    dsUrlImagem = new Image()
    modalidades : Array<ModalidadeModel>;
    dsRetiradaKits: string;
    regulamento: string;
    percursos: Array<Percurso>
}

export class Percurso{
    modalidade: string
    mapa: string
}