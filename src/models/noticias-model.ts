export class NoticiasModel{
    postId: number;
    title: string;
    link: string;
    comments: number;
    excerpt: string;
    content: string;
    fullText: string;
    date = new Date();
    creator: string;
    imageurl: string;
    category: string;
    categoryCor: string;
}

export class NoticiasEasyLoadModel{
    postId: number;
    title: string;
    link: string;
    content: string;
    imageurl: string;
}
