// import { Endereco } from './endereco-model'

export class User{
    idUser: string = '';
    name: string = '';
    docNumber: string = '';
    docType: string = '';
    email: string = '';
    password: any = null;
    dataNascimento = new Date();
    celular: string = '';
    residencial: string = '';
    sexo: string = '';
    pais: string = '';
    dataCadastro = new Date();
    avatar: string = '';
    // endereco =  new Endereco();
    token: string = '';
    facebook = true;
    sms = true;
    ReceiveEmail = true;
    specialNeeds = false;

    parentName = "";
    parentContact = "";
}
