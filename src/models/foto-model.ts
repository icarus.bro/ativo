
export class FotoModel{
    idUsuario: number;
    idEvento: number;
    nomeEvento: string;
    dataEvento: Date;
    local: string;
    numeroPeito: number;
    modalidade: string;
    fotos: Array<Foto>;
    ourocard: any;
    url: any;

}

export class Foto{
    id: number;
    url: string;
}
