
export class CalendarioEventosModel{
    idTipoEvento: number;
    dsTipoEvento: string;
    contador: number;
    thumbTipoEvento: string;
    iconeTipoEvento: string;
}