import { EventoAtleta } from './evento-atleta-model'
import { Pagamento } from './pagamento-model'

export class Pedido {
    id: number;
    data = new Date();
    idPedidoEvento: number; //não entendi esse campo, repete o id do evento. Parabens aos DBA`s, guardando informações repetidas sem necessidade;
    taxaTotal: number;
    quantidadeEvento: number;
    totalPedido: number;
    recibo: string;
    status:string;
    idUsuario: number;

    eventoAtletas = Array<EventoAtleta>();
    pagamentos = Array<Pagamento>();
    
}