export class AtletaPosicaoModel{
    idUsuario: number;
    idEvento: number;
    idResultado: number;
    nome: string;
    sexo: string;
    numPeito: number;
    modalidade: string;
    percurso: number;
    categoria: string;
    equipe: string;
    tempoTotal: string;
    tempoBruto: string;
    pace: string;
    itens = new Itens();
    classificacaoTotal: number;
}

class Itens{
  classificacaoSexo: number;
  classificacaoCategoria: number;
  classificacaoTotal: number;
  equipe: string;
  velocidadeMedia: number;
}
