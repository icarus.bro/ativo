import { NgModule } from '@angular/core';
import { ParalaxHeaderDirective } from './paralax-header/paralax-header';
@NgModule({
	declarations: [ParalaxHeaderDirective],
	imports: [],
	exports: [ParalaxHeaderDirective]
})
export class DirectivesModule {}
